Procvic.DataTables = Procvic.DataTables || {};

Procvic.DataTables.DataTable = function($node) {
    this.node = $node;

    this.node.dataTable({
        "aoColumns": this.getNotSortableColumns(),
        "responsive" : true,
        // @todo better language handling
        "language" : {
            "sProcessing":   "Provádím...",
            "sLengthMenu":   "Zobraz záznamů _MENU_",
            "sZeroRecords":  "Žádné záznamy nebyly nalezeny",
            "sInfo":         "Zobrazuji _START_ až _END_ z celkem _TOTAL_ záznamů",
            "sInfoEmpty":    "Zobrazuji 0 až 0 z 0 záznamů",
            "sInfoFiltered": "(filtrováno z celkem _MAX_ záznamů)",
            "sInfoPostFix":  "",
            "sSearch":       "Hledat:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "První",
                "sPrevious": "Předchozí",
                "sNext":     "Další",
                "sLast":     "Poslední"
            }
        }
    });
};

/**
 * @return array
 */
Procvic.DataTables.DataTable.prototype.getNotSortableColumns = function() {
    var notSortableColumns = [];

    this.node.find('th').each(function () {
        if ($(this).hasClass('grid-col-actions')) {
            notSortableColumns.push({ "bSortable": false });
        } else {
            notSortableColumns.push(null);
        }
    });

    return notSortableColumns;
};
