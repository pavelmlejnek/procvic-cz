// live preview for exercise and answers
$(document).ready(function(){
    $(".live-preview").on("keyup", function(){
        var val = $(this).val();
        val = val.replace(/(?:\r\n|\r|\n)/g, '<br />');
        var data = $(this).data("to");
        $("*[data-source='" + data + "']").html(val);
        MathJax.Hub.Queue(["Typeset",MathJax.Hub]); // reload MathJax
    });
});
