<?php

namespace Procvic\Traits;

/**
 * @package    Procvic
 * @subpackage Traits
 * @SuppressWarnings(PHPMD)
 */
trait TRequireLogin
{
    public function checkRequirements($element)
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect(':Sign:in');
        }
    }
}
