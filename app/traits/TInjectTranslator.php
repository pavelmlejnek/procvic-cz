<?php

namespace Procvic\Traits;

/**
 * @package    Procvic
 * @subpackage Traits
 * @SuppressWarnings(PHPMD)
 */
trait TInjectTranslator
{

    /**
     * @var string
     * @persistent
     */
    public $locale;

    /**
     * @var \Kdyby\Translation\Translator
     * @inject
     */
    public $translator;
}
