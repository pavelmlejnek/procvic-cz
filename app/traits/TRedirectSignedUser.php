<?php

namespace Procvic\Traits;

/**
 * @package    Procvic
 * @subpackage Traits
 * @SuppressWarnings(PHPMD)
 */
trait TRedirectSignedUser
{
    protected function beforeRender()
    {
        parent::beforeRender();

        if ($this->user->isLoggedIn()) {
            $this->redirect('Homepage:');
        }
    }
}
