<?php

namespace Procvic\Listeners;

use Nette;
use Nette\Object;
use Kdyby\Events\Subscriber;
use Nette\Application\LinkGenerator;
use Procvic\Sendgrid\SendgridMailer;

/**
 * Class which contains a bunch of events which are hooked into
 * several user actions.
 *
 * @package    Procvic
 * @subpackage Listeners
 */
class UserListener extends Object implements Subscriber
{
    /**
     * @var \Nette\Application\UI\ITemplate
     */
    private $templateFactory;


    /**
     * @var \Nette\Application\LinkGenerator
     */
    private $linkGenerator;


    /**
     * @var \Procvic\Sendgrid\SendgridMailer
     */
    private $mailer;


    /**
     * @param \Nette\Application\UI\ITemplateFactory $templateFactory
     * @param \Nette\Application\LinkGenerator       $linkGenerator
     * @param \Procvic\Sendgrid\SendgridMailer       $sendgrid
     */
    public function __construct(
        Nette\Application\UI\ITemplateFactory $templateFactory,
        LinkGenerator $linkGenerator,
        SendgridMailer $sendgrid
    ) {
        $this->templateFactory = $templateFactory;
        $this->linkGenerator = $linkGenerator;
        $this->mailer = $sendgrid;
    }


    /**
     * Return the list of events subscribed to this class.
     *
     * @return array The list of events subscribed to this class.
     */
    public function getSubscribedEvents()
    {
        return [
            'Procvic\Model\Facades\UserFacade::onForgottenPasswordRequest',
            'Procvic\Model\Facades\UserFacade::onRegistrationWithPasswordCompleted' => 'sendVerificationEmail',
            'Procvic\Model\Facades\UserFacade::onResendVerificationEmail' => 'sendVerificationEmail'
        ];
    }


    /**
     * Event which is dispatched immediately after a user send a request
     * for forgotten password change. It sends e-mail with hash.
     *
     * @param string $email
     * @param string $hash
     */
    public function onForgottenPasswordRequest($email, $hash)
    {
        $template = $this->templateFactory->createTemplate();
        $template->setFile(__dir__ . '/../templates/emails/forgottenPasswordChange.latte');

        $template->email = $email;
        $template->hash = $hash;
        $template->_control = $this->linkGenerator; // @todo waiting for better UI in Nette 2.4

        $message = new \SendGrid\Email();
        $message->addTo($email)
            ->setFrom('podpora@procvic.cz') // @todo maybe it would be better to set from in neon file
            ->setFromName('Procvič.cz') // @todo maybe it would be better to set from in neon file
            ->setSubject('Zapomenuté heslo') // @todo translate the e-mail subject
            ->setHtml($template);

        $this->mailer->send($message);
    }


    /**
     * @param string $email
     * @param string $hash
     */
    public function sendVerificationEmail($email, $hash)
    {
        $template = $this->templateFactory->createTemplate();
        $template->setFile(__dir__ . '/../templates/emails/verifyEmail.latte');

        $template->email = $email;
        $template->hash =$hash;
        $template->_control = $this->linkGenerator; // @todo waiting for better UI in Nette 2.4

        $message = new \SendGrid\Email();
        $message->addTo($email)
            ->setFrom('podpora@procvic.cz') // @todo maybe it would be better to set from in neon file
            ->setFromName('Procvič.cz') // @todo maybe it would be better to set from in neon file
            ->setSubject('Ověření e-mailu') // @todo translate the e-mail subject
            ->setHtml($template);

        $this->mailer->send($message);
    }
}
