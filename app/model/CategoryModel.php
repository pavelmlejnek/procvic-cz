<?php

namespace Procvic\Model;

use Nette\Object;
use Nette\Database\Connection;

/**
 * Model class for category of exercises.
 *
 * @package Procvic
 * @subpackage Model
 */
class CategoryModel extends Object
{
    /**
     * @var \Nette\Database\Connection
     */
    private $database;


    /**
     * @param \Nette\Database\Connection $database
     */
    public function __construct(Connection $database)
    {
        $this->database = $database;
    }


    /**
     * Return tree of categories for representation.
     *
     * `HAVING COUNT(e.exercise) > 6` is for fixed feature select last
     * unused exercise in SQL function `get_next_random_exercise_id`
     *
     * @param  int $categoryId Identifier for start listing of tree.
     * @return array           Tree of categories.
     */
    public function getTree($categoryId)
    {
        $items = [];
        $query = '
            SELECT c.*, COUNT(cat.exercise) as count_exercises
            FROM categories c
                LEFT JOIN exercises_categories cat ON cat.category = c.id
                LEFT JOIN exercises e ON e.id = cat.exercise
            WHERE e.authorize = 1
            GROUP BY c.id
            HAVING COUNT(cat.exercise) > 6
        ';
        $categories = $this->database->query($query)->fetchAll();

        foreach ($categories as $category) {
            if ($category->id == $category->parent && $categoryId == $category->id) {
                $this->insertSubCategoriesForTree(
                    $items,
                    $categories,
                    $category->id,
                    0
                );
            }
        }

        return $items;
    }


    /**
     * Recursion generate subcategories for tree
     */
    private function insertSubCategoriesForTree(&$items, $categories, $parentId, $depth)
    {
        foreach ($categories as $category) {
            $isBaseCategory = $category->id == $parentId;

            if ($category->parent == $parentId && !$isBaseCategory) {
                $items[$category->id] = [
                    'item' => $category,
                    'depth' => $depth
                ];

                $this->insertSubCategoriesForTree(
                    $items,
                    $categories,
                    $category->id,
                    $depth + 1
                );
            }
        }
    }


    public function getPrimaryCategories()
    {
        $query = '
            SELECT
                c.*,
                (
                    SELECT COUNT(*)
                    FROM exercises_categories cat
                        JOIN exercises e ON e.id = cat.exercise AND e.authorize = 1
                    WHERE cat.category = c.id
                ) as count_exercises
            FROM categories c
            WHERE c.id = c.parent
        ';
        $categories = $this->database->query($query)->fetchAll();

        $items = [];
        foreach ($categories as $category) {
            $items[$category['id']] = $category;
        }

        return $items;
    }
}
