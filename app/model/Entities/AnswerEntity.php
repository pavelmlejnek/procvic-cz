<?php

namespace Procvic\Model\Entities;

use Nette;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\IdentifiedEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="exercises_answers")
 */
class AnswerEntity extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="string")
     */
    private $text;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $success;

    /**
     * @ORM\ManyToOne(targetEntity="\Procvic\Model\Entities\ExerciseEntity", inversedBy="answers")
     */
    private $exercise;


    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }


    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = (string) $text;
    }


    /**
     * @return int
     */
    public function getSuccess()
    {
        return $this->success;
    }


    /**
     * @param int $success
     */
    public function setSuccess($success)
    {
        $this->success = (int) $success;
    }


    /**
     * @return int
     */
    public function getExercise()
    {
        return $this->exercise;
    }


    /**
     * @param int $exercise
     */
    public function setExercise($exercise)
    {
        $this->exercise = $exercise;
    }


    public function __toString()
    {
        return $this->getText();
    }
}
