<?php

namespace Procvic\Model\Entities;

use Nette;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\IdentifiedEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="exercises")
 */
class ExerciseEntity extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="string")
     */
    private $text;

    /**
     * @ORM\Column(type="string")
     */
    private $note;

    /**
     * @ORM\Column(type="integer", length=100)
     */
    private $category;

    /**
     * @ORM\Column(type="string")
     */
    private $level;

    /**
     * @ORM\OneToMany(targetEntity="\Procvic\Model\Entities\AnswerEntity", mappedBy="exercise", cascade={"persist"})
     */
    private $answers;

    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }


    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }


    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = (string) $text;
    }


    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }


    /**
     * @param string $note
     */
    public function setNote($note)
    {
        $this->note = (string) $note;
    }


    /**
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }


    /**
     * @param int $category
     */
    public function setCategory($category)
    {
        $this->category = (int) $category;
    }


    /**
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }


    /**
     * @param string $level
     */
    public function setLevel($level)
    {
        $this->level = (string) $level;
    }


    public function getAnswers()
    {
        return $this->answers;
    }


    public function __toString()
    {
        return $this->getText();
    }
}
