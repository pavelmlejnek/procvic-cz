<?php

namespace Procvic\Model\Entities;

use Nette;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\IdentifiedEntity;
use Nette\Security\IIdentity;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class UserEntity extends IdentifiedEntity implements IIdentity
{
    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';

    /**
     * @ORM\Column(type="string", unique=true, length=100)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $role;


    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }


    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = (string) $email;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = (string) $name;
    }


    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }


    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = (string) $surname;
    }


    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }


    /**
     * @param string password
     */
    public function setPassword($password)
    {
        $this->password = (string) $password;
    }


    /**
     * @return string
     * @todo   rewrite using enum and doctrine types
     */
    public function getRole()
    {
        return $this->role;
    }


    /**
     * @param string role
     * @todo  rewrite using enum and doctrine types
     */
    public function setRole($role)
    {
        if (!in_array($role, [self::ROLE_USER, self::ROLE_ADMIN])) {
            throw new \InvalidArgumentException('Invalid role has been given.');
        }

        $this->role = $role;
    }


    /**
     * @return array
     */
    public function getRoles()
    {
        return [$this->getRole()];
    }


    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->getName() && $this->getSurname() !== '' ?
            $this->getName() . ' ' . $this->getSurname() : $this->getEmail();
    }


    public function __toString()
    {
        return $this->getDisplayName();
    }
}
