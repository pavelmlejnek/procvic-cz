<?php

namespace Procvic\Model\Entities;

use Nette;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\IdentifiedEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="categories")
 */
class CategoryEntity extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="string")
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $lang = 'cs';

    /**
     * @ORM\Column(type="integer", length=100)
     */
    private $parent;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;


    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = (string) $name;
    }


    /**
     * @return int
     */
    public function getParent()
    {
        return $this->parent;
    }


    /**
     * @param int $parent
     */
    public function setParent($parent)
    {
        $this->parent = (int) $parent;
    }


    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }


    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = (string) $label;
    }


    public function __toString()
    {
        return $this->getName();
    }
}
