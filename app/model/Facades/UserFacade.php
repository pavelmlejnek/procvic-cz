<?php

namespace Procvic\Model\Facades;

use Kdyby;
use Nette\Object;
use Kdyby\Doctrine\EntityDao;
use Kdyby\Doctrine\EntityManager;
use Procvic\Model\Entities\UserEntity;

/**
 * User facade
 *
 * @package Procvic
 * @subpackage Model\Facades
 */
class UserFacade extends Object
{
    /**
     * @var \Kdyby\Doctrine\EntityDao
     */
    private $dataAccessObject;


    /**
     * @param \Kdyby\Doctrine\EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->dataAccessObject = $entityManager->getDao(
            UserEntity::getClassName()
        );
    }


    /**
     * @param array $criteria
     * @param array $orderBy
     * @return mixed|null|object
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->dataAccessObject->findOneBy($criteria, $orderBy);
    }


    /**
     * @return mixed|null|object
     */
    public function findAll()
    {
        return $this->dataAccessObject->findAll();
    }


    /**
     * @todo find better solution
     */
    public function save($entity = null, $relations = null)
    {
        return $this->dataAccessObject->save($entity, $relations);
    }


    public function delete($entity, $relations = null)
    {
        return $this->dataAccessObject->delete($entity, $relations);
    }
}
