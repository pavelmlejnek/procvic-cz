<?php

namespace Procvic\Model\Facades;

use Kdyby;
use Nette\Object;
use Kdyby\Doctrine\EntityDao;
use Kdyby\Doctrine\EntityManager;
use Procvic\Model\Entities\AnswerEntity;

/**
 * Exercise facade
 *
 * @package Procvic
 * @subpackage Model\Facades
 */
class AnswerFacade extends Object
{
    /**
     * @var \Kdyby\Doctrine\EntityDao
     */
    private $dataAccessObject;


    /**
     * @param \Kdyby\Doctrine\EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->dataAccessObject = $entityManager->getDao(
            AnswerEntity::getClassName()
        );
    }


    /**
     * @todo find better solution
     */
    public function save($entity = null, $relations = null)
    {
        return $this->dataAccessObject->save($entity, $relations);
    }
}
