<?php

namespace Procvic\Model\Facades;

use Kdyby;
use Nette\Object;
use Kdyby\Doctrine\EntityDao;
use Kdyby\Doctrine\EntityManager;
use Procvic\Model\Entities\ExerciseEntity;

/**
 * Exercise facade
 *
 * @package Procvic
 * @subpackage Model\Facades
 */
class ExerciseFacade extends Object
{
    /**
     * @var \Kdyby\Doctrine\EntityDao
     */
    private $dataAccessObject;


    /**
     * @param \Kdyby\Doctrine\EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->dataAccessObject = $entityManager->getDao(
            ExerciseEntity::getClassName()
        );
    }


    /**
     * @param array $criteria
     * @param array $orderBy
     * @return mixed|null|object
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->dataAccessObject->findOneBy($criteria, $orderBy);
    }


    /**
     * @todo find better solution
     */
    public function save($entity = null, $relations = null)
    {
        return $this->dataAccessObject->save($entity, $relations);
    }
}
