<?php

namespace Procvic\Model\Facades;

use Kdyby;
use Nette\Object;
use Kdyby\Doctrine\EntityDao;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Persistence\ObjectDao;
use Procvic\Model\Entities\CategoryEntity;

/**
 * Category facade
 *
 * @package Procvic
 * @subpackage Model\Facades
 */
final class CategoryFacade extends Object
{
    /**
     * @var \Kdyby\Doctrine\EntityDao
     */
    private $dataAccessObject;


    /**
     * @param \Kdyby\Doctrine\EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->dataAccessObject = $entityManager->getDao(
            CategoryEntity::getClassName()
        );
    }


    /**
     * @param array $criteria
     * @param array $orderBy
     * @return mixed|null|object
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->dataAccessObject->findOneBy($criteria, $orderBy);
    }


    /**
     * @return mixed|null|object
     */
    public function findAll()
    {
        return $this->dataAccessObject->findAll();
    }


    /**
     * @todo find better solution
     */
    public function save($entity = null, $relations = null)
    {
        return $this->dataAccessObject->save($entity, $relations);
    }


    /**
     * @param  object|array|\Traversable          $entity
     * @param  boolean                            $flush
     * @throws InvalidArgumentException
     */
    public function delete($entity, $flush = ObjectDao::FLUSH)
    {
        $this->dataAccessObject->delete($entity, $flush);
    }


    /**
     * Generate array of all categories in format id => name
     *
     * @return array
     */
    public function getCategoriesForSelect()
    {
        $items = [];
        $categories = $this->findAll();

        foreach ($categories as $category) {
            if ($category->getId() == $category->getParent()) {
                $items[$category->getId()] = $category->getName();

                $this->insertSubCategoriesForSelect(
                    $items,
                    $categories,
                    $category->getId(),
                    $category->getName()
                );
            }
        }

        return $items;
    }


    /**
     * Recursion generate subcategories
     */
    private function insertSubCategoriesForSelect(&$items, $categories, $parentId, $baseName)
    {
        foreach ($categories as $category) {
            $isBaseCategory = $category->getId() == $parentId;

            if ($category->getParent() == $parentId && !$isBaseCategory) {
                $name = $baseName . ' / ' . $category->getName();
                $items[$category->getId()] = $name;

                $this->insertSubCategoriesForSelect(
                    $items,
                    $categories,
                    $category->getId(),
                    $name
                );
            }
        }
    }
}
