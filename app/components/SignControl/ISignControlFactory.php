<?php

namespace Procvic\Components\SignControl;

/**
 * @package    Procvic
 * @subpackage Components\SignInForm
 */
interface ISignControlFactory
{
    /**
     * @return \Procvic\Components\SignControl\SignControl
     */
    public function create();
}
