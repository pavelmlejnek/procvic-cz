<?php

namespace Procvic\Components\SignControl;

use Kdyby\Autowired\AutowireComponentFactories;
use Nette\Object;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\User;
use Procvic\Components\SignControl\Controls\IFacebookSignInControlFactory;
use Procvic\Components\SignControl\Controls\IPasswordRecoveryControlFactory;
use Procvic\Components\SignControl\Controls\IPasswordSignInControlFactory;
use Procvic\Components\SignControl\Controls\IPasswordSignUpControlFactory;
use Tracy\Debugger;
use Tracy\ILogger;

/**
 * @package    Procvic
 * @subpackage ApplicationModule\Forms
 * @SuppressWarnings(PHPMD)
 */
class SignControl extends Control
{
    use AutowireComponentFactories;

    /**
     * @var array
     */
    public $onSignIn = [];


    /**
     * @var array
     */
    public $onSignUp = [];


    /**
     * @var array
     */
    public $onSignInError = [];


    /**
     * @var array
     */
    public $onPasswordRecovery = [];


    /**
     * @var array
     */
    public $onPasswordRecoveryFailed = [];


    /**
     * @var array
     */
    public $onFacebookMissingEmailPermission = [];


    /**
     * @var \Tracy\ILogger
     */
    private $logger;


    /**
     * @param \Tracy\ILogger $logger
     */
    public function __construct(ILogger $logger)
    {
        $this->logger = $logger;
    }


    /**
     * @param \Procvic\Components\SignControl\Controls\IPasswordSignInControlFactory $factory
     * @return \Procvic\Components\SignControl\Controls\PasswordSignInControl
     */
    public function createComponentPasswordSignInControl(IPasswordSignInControlFactory $factory)
    {
        $control = $factory->create();
        $control->onSignIn[] = function ($email) {
            $this->logger->log(
                sprintf('User "%s" successfully signed in.', $email),
                \Monolog\Logger::INFO
            );
            $this->onSignIn();
        };
        $control->onSignInError[] = function ($email) {
            $this->logger->log(
                sprintf('User "%s" tried to sign in with wrong credentials.', $email),
                \Monolog\Logger::INFO
            );
            $this->onSignInError();
        };


        return $control;
    }


    /**
     * @param \Procvic\Components\SignControl\Controls\IPasswordSignInControlFactory $factory
     * @return \Procvic\Components\SignControl\Controls\PasswordSignInControl
     */
    public function createComponentPasswordSignUpControl(IPasswordSignUpControlFactory $factory)
    {
        $control = $factory->create();
        $control->onSignUp[] = function ($email) {
            $this->logger->log(
                sprintf('User "%s" successfully registered.', $email),
                \Monolog\Logger::INFO
            );
            $this->onSignUp();
        };
        $control->onDuplicateEmailError[] = function ($email) {
            $this->logger->log(
                sprintf('User tried to register with existing e-mail "%s".', $email),
                \Monolog\Logger::INFO
            );
        };

        return $control;
    }


    /**
     * @param \Procvic\Components\SignControl\Controls\IFacebookSignInControlFactory $factory
     * @return \Procvic\Components\SignControl\Controls\FacebookSignInControl
     */
    public function createComponentFacebookSignInControl(IFacebookSignInControlFactory $factory)
    {
        $control = $factory->create();
        $control->onError[] = function () {
            $this->logger->log(
                'There was a problem while signing in via Facebook.',
                \Monolog\Logger::INFO
            );
        };
        $control->onAccountMerge[] = function ($email) {
            $this->logger->log(
                sprintf('User "%s" successfully merged his existing account with Facebook account.', $email),
                \Monolog\Logger::INFO
            );
        };
        $control->onRegistration[] = function ($email) {
            $this->logger->log(
                sprintf('User "%s" successfully registered via Facebook.', $email),
                \Monolog\Logger::INFO
            );
        };
        $control->onSignIn[] = function ($email) {
            $this->logger->log(
                sprintf('User "%s" successfully signed in via Facebook', $email),
                \Monolog\Logger::INFO
            );
        };
        $control->onMissingEmailPermission[] = function ($facebookId) {
            $this->logger->log(
                sprintf('User with facebook id "%i" did not provide the permission for accessing e-mail.', $facebookId),
                \Monolog\Logger::INFO
            );
            $this->onFacebookMissingEmailPermission();
        };

        return $control;
    }


    /**
     * @param \Procvic\Components\SignControl\Controls\IPasswordRecoveryControlFactory $factory
     * @return \Procvic\Components\SignControl\Controls\PasswordRecoveryControl
     */
    public function createComponentPasswordRecoveryControl(IPasswordRecoveryControlFactory $factory)
    {
        $control = $factory->create();
        $control->onRecover[] = function ($email) {
            $this->logger->log(
                sprintf('User "%s" requested new password.', $email),
                \Monolog\Logger::INFO
            );
            $this->onPasswordRecovery();
        };
        $control->onNonExistingEmail = function ($email) {
            $this->logger->log(
                sprintf('User tried to request new password with non-existing e-mail "%s".', $email),
                \Monolog\Logger::INFO
            );
            $this->onPasswordRecoveryFailed();
        };

        return $control;
    }


    public function renderSignIn()
    {
        $this->template->setFile(__DIR__ . '/templates/signIn.latte');
        $this->template->render();
    }


    public function renderSignUp()
    {
        $this->template->setFile(__DIR__ . '/templates/signUp.latte');
        $this->template->render();
    }


    public function renderPasswordRecovery()
    {
        $this->template->setFile(__DIR__ . '/templates/passwordRecovery.latte');
        $this->template->render();
    }
}
