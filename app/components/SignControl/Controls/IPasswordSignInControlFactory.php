<?php

namespace Procvic\Components\SignControl\Controls;

interface IPasswordSignInControlFactory
{
    /**
     * @return \Procvic\Components\SignControl\Controls\PasswordSignInControl
     */
    public function create();
}
