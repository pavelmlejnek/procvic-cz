<?php

namespace Procvic\Components\SignControl\Controls;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\User;
use Kdyby\Translation\Translator;
use Procvic\Model\Facades\UserFacade;

/**
 * @package    Procvic
 * @subpackage Components\SignControl\Controls
 * @SuppressWarnings(PHPMD)
 */
class PasswordSignInControl extends Control
{
    /**
     * @var array
     */
    public $onSignIn = [];


    /**
     * @var array
     */
    public $onSignInError = [];


    /**
     * @var \Nette\Security\User
     */
    private $user;


    /**
     * @var \Procvic\Model\Facades\UserFacade
     */
    private $userFacade;


    /**
     * @var \Kdyby\Translation\ITranslator
     */
    private $translator;


    /**
     * @param \Nette\Security\User              $user
     * @param \Procvic\Model\Facades\UserFacade $userFacade
     * @param \Kdyby\Translation\Translator     $translator
     */
    public function __construct(User $user, UserFacade $userFacade, Translator $translator)
    {
        $this->user = $user;
        $this->userFacade = $userFacade;
        $this->translator = $translator;
    }


    /**
     * Create form which serves for signing in
     *
     * @return \Nette\Application\UI\Form
     */
    public function createComponentPasswordSignInForm()
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        $form->addText('email', 'strings.signInForm.email.label')
            ->addRule(Form::EMAIL, 'strings.signInForm.email.invalid')
            ->setRequired('strings.signInForm.email.required');
        $form->addPassword('password', 'strings.signInForm.password.label')
            ->setRequired('strings.signInForm.password.required');
        $form->addCheckbox('remember', 'strings.signInForm.remember.label')
            ->setDefaultValue(true);
        $form->addSubmit('send', 'strings.signInForm.signIn.value');

        $form->onSuccess[] = function (Form $form) {
            $this->user->setExpiration('14 days', false);

            try {
                $this->user->login($form->values->email, $form->values->password);
                $this->onSignIn($form->values->email); //fire event
            } catch (AuthenticationException $e) {
                $this->onSignInError($form->values->email); // fire event
            }
        };

        return $form;
    }


    /**
     * Render sign in form
     */
    public function render()
    {
        $this->template->setFile(__DIR__ . '/templates/passwordSignInForm.latte');
        $this->template->render();
    }
}
