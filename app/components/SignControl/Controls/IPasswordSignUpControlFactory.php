<?php

namespace Procvic\Components\SignControl\Controls;

interface IPasswordSignUpControlFactory
{
    /**
    * @return \Procvic\Components\SignControl\Controls\PasswordSignUpControl
    */
    public function create();
}
