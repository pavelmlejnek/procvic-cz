<?php

namespace Procvic\Components\SignControl\Controls;

use Nette\Database\UniqueConstraintViolationException;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Procvic\Model\Identity;
use Kdyby\Translation\Translator;
use Procvic\Model\Facades\UserFacade;

/**
 * @package    Procvic
 * @subpackage Components\SignControl\Controls
 * @SuppressWarnings(PHPMD)
 */
class PasswordSignUpControl extends Control
{
    /**
     * @var array
     */
    public $onSignUp = [];


    /**
     * @var array
     */
    public $onDuplicateEmailError = [];


    /**
     * @var \Nette\Security\User
     */
    private $user;


    /**
     * @var \Procvic\Model\Facades\UserFacade
     */
    private $userFacade;


    /**
     * @var \Kdyby\Translation\ITranslator
     */
    private $translator;


    /**
     * @param \Nette\Security\User              $user
     * @param \Procvic\Model\Facades\UserFacade $userFacade
     * @param \Kdyby\Translation\Translator     $translator
     */
    public function __construct(User $user, UserFacade $userFacade, Translator $translator)
    {
        $this->user = $user;
        $this->userFacade = $userFacade;
        $this->translator = $translator;
    }


    /**
     * @return \Nette\Application\UI\Form
     */
    public function createComponentPasswordSignUpForm()
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        $form->addText('email', 'strings.signUpForm.email.label')
            ->addRule(Form::EMAIL, 'strings.signUpForm.email.invalid')
            ->setRequired('strings.signUpForm.email.required');
        $form->addPassword('password', 'strings.signUpForm.password.label')
            ->addRule(Form::MIN_LENGTH, 'strings.signUpForm.password.minLength', 4)
            ->setRequired('strings.signUpForm.password.required');
        $form->addPassword('password_repeat', 'strings.signUpForm.passwordRepeat.label')
            ->addRule(Form::EQUAL, 'strings.signUpForm.passwordRepeat.notEqual', $form['password']);
        $form->addCheckbox('agree')
            ->addRule(Form::EQUAL, null, true);
        $form->addSubmit('send', 'strings.signUpForm.send.value');

        $form->onSuccess[] = function (Form $form, $values) {
            try {
                $newUser = $this->userFacade->registerWithPassword($values->email, $values->password);
                $this->user->login(new Identity($newUser->id));
                $this->onSignUp($values->email);
            } catch (UniqueConstraintViolationException $e) {
                $form->addError($this->translator->translate('strings.registration.error.emailAlreadyExists'));
                $this->onDuplicateEmailError($values->email);
            }
        };

        return $form;
    }


    /**
     * Render sign up form
     */
    public function render()
    {
        $this->template->setFile(__DIR__ . '/templates/passwordSignUpForm.latte');
        $this->template->render();
    }
}
