<?php

namespace Procvic\Components\SignControl\Controls;

interface IFacebookSignInControlFactory
{
    /**
     * @return \Procvic\Components\SignControl\Controls\FacebookSignInControl
     */
    public function create();
}
