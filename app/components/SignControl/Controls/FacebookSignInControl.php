<?php

namespace Procvic\Components\SignControl\Controls;

use Kdyby\Facebook\Facebook;
use Kdyby\Facebook\FacebookApiException;
use Nette\Object;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Procvic\Components\SignInControl\Exceptions\MissingPermissionException;
use Procvic\Model\Identity;
use Kdyby\Translation\Translator;
use Procvic\Model\Facades\UserFacade;

/**
 * @package    Procvic
 * @subpackage ApplicationModule\Forms
 * @SuppressWarnings(PHPMD)
 */
class FacebookSignInControl extends Control
{
    /**
     * @var array
     */
    public $onSignIn = [];


    /**
     * @var array
     */
    public $onError = [];


    /**
     * @var array
     */
    public $onAccountMerge = [];


    /**
     * @var array
     */
    public $onRegistration = [];


    /**
     * @var array
     */
    public $onMissingEmailPermission = [];


    /**
     * @var \Nette\Security\User
     */
    private $user;


    /**
     * @var \Procvic\Model\Facades\UserFacade
     */
    private $userFacade;


    /**
     * @var \Kdyby\Translation\ITranslator
     */
    private $translator;


    /**
     * @var \Kdyby\Facebook\Facebook
     */
    private $facebook;


    /**
     * @param \Nette\Security\User              $user
     * @param \Procvic\Model\Facades\UserFacade $userFacade
     * @param \Kdyby\Translation\Translator     $translator
     */
    public function __construct(User $user, Facebook $facebook, UserFacade $userFacade, Translator $translator)
    {
        $this->user = $user;
        $this->facebook = $facebook;
        $this->userFacade = $userFacade;
        $this->translator = $translator;
    }


    /**
     * @return \Kdyby\Facebook\Dialog
     */
    public function createComponentFacebook()
    {
        $dialog = $this->facebook->createDialog('login');

        $dialog->onResponse[] = function() use ($dialog) {
            $facebook = $dialog->getFacebook();

            if (!$facebook->getUser()) {
                $this->flashMessage($this->translator->translate('facebook.problem'));
                $this->onError();
                return;
            }

            try {
                $user = $facebook->api('/me');

                // we really need user's email, so if he didn't grant access to it, we can't log him in
                if (empty($user->email)) {
                    throw new MissingPermissionException(
                        'The permission for accessing the email was not provided by the user.'
                    );
                }

                // if user exists and is logging in for the first time via Facebook, we should merge accounts
                if ($this->userFacade->getByEmail($user->email) &&
                    !$this->userFacade->isFacebookUser($user->email)) {
                    $existingUser = $this->userFacade->mergeWithFacebook($user->email, $user);
                    $this->onAccountMerge($user->email);
                }

                // if user does not exist in database register him via the Facebook data
                if (!$existingUser = $this->userFacade->getByFacebookId($facebook->getUser())) {
                    $existingUser = $this->userFacade->registerWithFacebook($facebook->getUser(), $user);
                    $this->onRegistration($user->email);
                }

                // update Facebook's access token for possible future need
                $this->userFacade->updateFacebookAccessToken($existingUser->id, $facebook->getAccessToken());

                // login the user
                $this->presenter->user->login(new Identity($existingUser->id));

                $this->onSignIn($existingUser->email);

            } catch (FacebookApiException $e) {
                $this->logger->log($e);
                $this->presenter->flashMessage(
                    $this->translator->translate('login.facebook.failed'),
                    'danger'
                );
            } catch (MissingPermissionException $e) {
                $this->onMissingEmailPermission($facebook->getUser());
            }
        };

        return $dialog;
    }


    /**
     * Render normal signing button
     */
    public function render()
    {
        $this->template->setFile(__DIR__ . '/templates/facebookSignInButton.latte');
        $this->template->render();
    }


    /**
     * Render large registration button
     */
    public function renderLargeButton()
    {
        $this->template->setFile(__DIR__ . '/templates/largeFacebookSignInButton.latte');
        $this->template->render();
    }
}
