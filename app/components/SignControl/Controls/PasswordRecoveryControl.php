<?php

namespace Procvic\Components\SignControl\Controls;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Kdyby\Translation\Translator;
use Procvic\Model\Facades\UserFacade;

/**
 * @package    Procvic
 * @subpackage Components\SignControl\Controls
 * @SuppressWarnings(PHPMD)
 */
class PasswordRecoveryControl extends Control
{
    /**
     * @var array
     */
    public $onRecover = [];


    /**
     * @var array
     */
    public $onNonExistingEmail = [];


    /**
     * @var \Procvic\Model\Facades\UserFacade
     */
    private $userFacade;


    /**
     * @var \Kdyby\Translation\ITranslator
     */
    private $translator;


    /**
     * @param \Procvic\Model\Facades\UserFacade $userFacade
     * @param \Kdyby\Translation\Translator     $translator
     */
    public function __construct(UserFacade $userFacade, Translator $translator)
    {
        $this->userFacade = $userFacade;
        $this->translator = $translator;
    }


    /**
     * @return \Nette\Application\UI\Form
     */
    public function createComponentPasswordRecoveryForm()
    {
        $form = new Form();
        $form->addText('email', 'E-mail:')
            ->addRule(Form::EMAIL, 'Zadej validní e-mail')
            ->setRequired();

        $form->addSubmit('send', 'Odeslat');

        $form->onSuccess[] = function (Form $form) {
            try {
                $this->userFacade->requestNewPassword($form->values->email);
                $this->onRecover($form->values->email);
            } catch (\InvalidArgumentException $e) {
                //$form->addError('E-mail neexistuje.'); @todo think about that a bit
                $this->onNonExistingEmail($form->values->email);
            }
        };

        return $form;
    }


    /**
     * Render sign up form
     */
    public function render()
    {
        $this->template->setFile(__DIR__ . '/templates/passwordRecoveryForm.latte');
        $this->template->render();
    }
}
