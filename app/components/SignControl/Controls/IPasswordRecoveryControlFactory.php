<?php

namespace Procvic\Components\SignControl\Controls;

interface IPasswordRecoveryControlFactory
{
    /**
     * @return \Procvic\Components\SignControl\Controls\PasswordRecoveryControl
     */
    public function create();
}
