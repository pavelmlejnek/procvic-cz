<?php

namespace Procvic\Components\SignInControl\Exceptions;

use Nette;
use Nette\InvalidStateException;

class MissingPermissionException extends InvalidStateException
{
}
