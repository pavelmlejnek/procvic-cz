<?php

namespace Procvic\Components\Forms\RegistrationForm;

use Nette\Object;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Procvic\ApplicationModule\Model\Managers\UserManager;
use Kdyby\Translation\Translator;

/**
* @package    Procvic
* @subpackage Components\Forms\RegisterForm
*/
final class RegistrationForm extends Control
{
    /**
    * @var \Procvic\Model\Managers\UserManager
    */
    private $userManager;


    /**
     *
     */
    private $translator;


    /**
    * @param \FinWatch\ApplicationModule\Model\Managers\UserManager $userManager
    */
    public function __construct(UserManager $userManager, Translator $translator)
    {
        $this->userManager = $userManager;
        $this->translator = $translator;
    }


    /**
    * @return \Nette\Application\UI\Form
    */
    public function createComponentForm()
    {
        $form = new Form();

        $form->setTranslator($this->translator);

        $form->addText('email', 'frontend-register.default.form_email')
            ->addRule(Form::EMAIL, 'frontend-register.default.form_validation_email_has_bad_format')
            ->setRequired('frontend-register.default.form_validation_email_is_required');

        $form->addPassword('password', 'frontend-register.default.form_password')
            ->addRule(Form::MIN_LENGTH, 'frontend-register.default.form_validation_password_is_too_short', 4)
            ->setRequired('frontend-register.default.form_validation_password_is_required');

        $form->addPassword('password_repeat', 'frontend-register.default.form_password_repeat')
            ->addRule(
                Form::EQUAL,
                'frontend-register.default.form_validation_password_does_not_match',
                $form['password']
            );

        $form->addCheckbox('agree')
            ->addRule(Form::EQUAL, 'Je potřeba souhlasit s podmínkami', true);

        $form->addSubmit('send', 'frontend-register.default.form_action_button');

        $form->onSuccess[] = $this->formSubmitted;

        return $form;
    }

    /**
    * @param \Nette\Application\UI\Form $form
    * @param \Nette\Utils\ArrayHash     $values
    */
    public function formSubmitted($form, $values)
    {
        try {
            $this->userManager->register($form->getValues());
        } catch (\PDOException $e) {
            $form->addError('Uživatel s tímto emailem je již registrován. Nezapomněl jste své heslo?');
        }

        $this->presenter->flashMessage(
            $this->translator->translate('frontend-register.default.flash_registration_completed'),
            'success'
        );

        $this->presenter->redirect(':Sign:in');

    }


    public function render()
    {
        $this->template->setFile(__DIR__ . '/registrationForm.latte');
        $this->template->render();
    }
}
