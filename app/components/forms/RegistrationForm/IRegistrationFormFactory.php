<?php

namespace Procvic\Components\Forms\RegistrationForm;

/**
* @package    Procvic
* @subpackage Components\Forms\RegistrationForm
*/
interface IRegistrationFormFactory
{
    /**
    * @return \Procvic\Components\Forms\RegistrationForm\RegistrationForm
    */
    public function create();
}
