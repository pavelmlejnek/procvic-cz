<?php

namespace Procvic\Traits;

use Parsedown;

/**
 * @package Procvic
 * @subpackage Traits
 */
trait RegisterMarkdownFilterTrait
{
    protected function createTemplate()
    {
        $template = parent::createTemplate();

        $template->addFilter('markdown', function ($string) {
            return Parsedown::instance()->text($string);
        });

        return $template;
    }
}
