<?php

namespace Procvic\ApplicationModule\Traits;

/**
 * @package Procvic
 * @subpackage ApplicationModule\Traits
 * @SuppressWarnings(PHPMD)
 */
trait RequireLoginTrait
{
    public function checkRequirements($element)
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }
}
