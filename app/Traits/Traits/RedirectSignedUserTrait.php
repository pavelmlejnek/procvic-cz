<?php

namespace Procvic\ApplicationModule\Traits;

/**
 * @package Procvic
 * @subpackage ApplicationModule\Traits
 * @SuppressWarnings(PHPMD)
 */
trait RedirectSignedUserTrait
{
    protected function beforeRender()
    {
        parent::beforeRender();

        if ($this->user->isLoggedIn()) {
            $this->redirect('Homepage:');
        }
    }
}
