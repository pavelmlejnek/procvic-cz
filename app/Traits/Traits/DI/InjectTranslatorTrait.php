<?php

namespace Procvic\ApplicationModule\Traits\DI;

use Kdyby\Translation\Translator;

/**
 * @package Procvic
 * @subpackage ApplicationModule\Traits
 * @SuppressWarnings(PHPMD)
 */
trait InjectTranslatorTrait
{

    /**
     * @var string
     * @persistent
     */
    public $locale;

    /**
     * @var \Kdyby\Translation\Translator
     */
    protected $translator;


    /**
     * @param \Kdyby\Translation\Translator $translator
     */
    public function injectTranslator(Translator $translator)
    {
        $this->translator = $translator;
    }
}
