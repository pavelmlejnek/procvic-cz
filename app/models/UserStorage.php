<?php

namespace Procvic\Model;

use Nette\Http\Session;
use Procvic\Model\Facades\UserFacade;

/**
 * Class UserStorage
 *
 * @package Procvic\Model
 */
class UserStorage extends \Nette\Http\UserStorage
{
    /**
     * @var \Procvic\Model\Facades\UserFacade
     */
    private $userFacade;


    /**
     * @param \Nette\Http\Session               $sessionHandler
     * @param \Procvic\Model\Facades\UserFacade $userFacade
     */
    public function __construct(Session $sessionHandler, UserFacade $userFacade)
    {
        parent::__construct($sessionHandler);
        $this->userFacade = $userFacade;
    }


    /**
     * @return \Nette\Security\IIdentity|NULL
     */
    public function getIdentity()
    {
        $identity = parent::getIdentity();

        if ($identity && $identity instanceof Identity && !$identity->isUpToDate()) {
            $this->userFacade->loadIdentity($identity);
        }

        return $identity;
    }
}
