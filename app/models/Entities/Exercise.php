<?php

namespace Procvic\Model\Entities;

use Nette\Object;
use Nette\Database\Table\ActiveRow;
use Procvic\Model\Repositories\ExerciseAnswerRepository;

/**
 * Class Exercise
 *
 * @package    Procvic
 * @subpackage Model\Entities
 * @SuppressWarnings(PHPMD)
 */
class Exercise extends Object
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $text;

    /**
     * @var array
     */
    public $answers = [];

    /**
     * @var string
     */
    public $note;

    /**
     * @var string
     */
    public $user;

    /**
     * @param \Nette\Database\Table\ActiveRow $row
     * @return static
     */
    public static function fromActiveRow(ActiveRow $row)
    {
        $exercise = new static;
        $exercise->id = $row->id;
        $exercise->text = $row->text;
        $exercise->note = $row->note;
        $exercise->user = $row->user;

        foreach ($row->related(ExerciseAnswerRepository::$table) as $answer) {
            $exercise->answers[] = $answer;
        }

        return $exercise;
    }
}
