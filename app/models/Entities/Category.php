<?php

namespace Procvic\Model\Entities;

use Nette\Object;
use Nette\Database\Table\ActiveRow;

/**
 * Class Category
 *
 * @package    Procvic
 * @subpackage Model\Entities
 * @SuppressWarnings(PHPMD)
 */
class Category extends Object
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $label;

    /**
     * @var integer
     */
    public $parent;

    /**
     * @var string
     */
    public $name;

    /**
     * @param \Nette\Database\Table\ActiveRow $row
     * @return static
     */
    public static function fromActiveRow(ActiveRow $row)
    {
        $category = new static;
        $category->id = $row->id;
        $category->label = $row->label;
        $category->parent = $row->parent;
        $category->name = $row->name;

        return $category;
    }
}
