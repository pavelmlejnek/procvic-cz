<?php

namespace Procvic\Model;

use Nette\Object;
use Nette\Database\Connection;

/**
 * Model class for exercise.
 *
 * @package Procvic
 * @subpackage Model
 */
class ExerciseModel extends Object
{
    /**
     * @var \Nette\Database\Connection
     */
    private $database;


    /**
     * @param \Nette\Database\Connection $database
     */
    public function __construct(Connection $database)
    {
        $this->database = $database;
    }


    /**
     * Get exercise
     *
     * @param int $exerciseID
     */
    public function get($exerciseID)
    {
        $query = 'SELECT * FROM exercises WHERE id = ?';
        $exercise = $this->database->query($query, $exerciseID)->fetch();

        $query = 'SELECT * FROM exercises_answers WHERE exercise = ?';
        $answers = $this->database->query($query, $exerciseID)->fetchAll();

        return [
            'question' => $exercise,
            'answers' => $answers,
        ];
    }


    /**
     * Save exercise with answers
     *
     * @param array $exercise
     */
    public function save(array $exercise)
    {
        $this->database->beginTransaction();

        $query = '
            INSERT INTO exercises (text, note, category, user, level, stamp)
            VALUES (?, ?, ?, ?, ?, NOW())
        ';
        $this->database->query(
            $query,
            $exercise['question']['text'],
            $exercise['question']['note'],
            (int) $exercise['question']['category'],
            (int) $exercise['question']['user'],
            $exercise['question']['level']
        );

        foreach ($exercise['answers'] as $answer) {
            $query = '
                INSERT INTO exercises_answers (exercise, text, correct)
                VALUES ((SELECT MAX(id) FROM exercises), ?, ?)
            ';
            $exercise = $this->database->query(
                $query,
                $answer['text'],
                (bool) $answer['correct']
            );
        }

        $this->database->commit();
    }
}
