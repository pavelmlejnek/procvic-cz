<?php

namespace Procvic\Model;

use Nette\Object;
use Nette\Security\IIdentity;

/**
 * Class Identity
 *
 * @package Procvic\Model
 */
class Identity extends Object implements IIdentity
{
    /**
     * @var integer
     */
    public $userId;


    /**
     * @var string
     */
    public $facebookId;


    /**
     * @var string
     */
    public $facebookAccessToken;


    /**
     * @var string
     */
    public $role;


    /**
     * @var string
     */
    public $email;


    /**
     * @var string
     */
    public $name;


    /**
     * @var string
     */
    public $surname;


    /**
     * @var string
     */
    public $about;


    /**
     * @var \Nette\Utils\DateTime
     */
    public $createdAt;


    /**
     * @var boolean
     */
    public $verified;


    /**
     * @var boolean
     */
    public $upToDate = false;


    /**
     * @param integer $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }


    /**
     * @return integer
     */
    public function getId()
    {
        return $this->userId;
    }


    /**
     * @return array
     */
    public function getRoles()
    {
        return [$this->role];
    }


    /**
     * @return string
     */
    public function getDisplayName()
    {
        if ($this->name && $this->surname) {
            return $this->name . ' ' . $this->surname;
        }

        return $this->email;
    }


    /**
     * @return boolean
     */
    public function isUpToDate()
    {
        return $this->upToDate;
    }


    /**
     * @return boolean
     */
    public function isFacebookUser()
    {
        return $this->facebookId ? true : false;
    }


    /**
     * @return array
     */
    public function __sleep()
    {
        return ['userId'];
    }


    public function __wakeup()
    {
        $this->upToDate = false;
    }
}
