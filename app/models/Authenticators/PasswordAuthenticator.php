<?php

namespace Procvic\Model\Authenticators;

use Nette\Object;
use Nette\Security\IAuthenticator;
use Nette\Security\AuthenticationException;
use Nette\Security\Passwords;
use Procvic\Model\Identity;
use Procvic\Model\Repositories\UserEmailRepository;
use Procvic\Model\Repositories\UserRepository;

/**
 * Manager class for working with users.
 *
 * @package Procvic
 * @subpackage Model
 * @SuppressWarnings(PHPMD)
 */
class PasswordAuthenticator extends Object implements IAuthenticator
{

    /**
     * @var \Procvic\Model\Repositories\UserRepository
     */
    private $userRepository;

    /**
     * @var \Procvic\Model\Repositories\UserEmailRepository
     */
    private $userEmailRepository;


    /**
     * @param \Procvic\Model\Repositories\UserRepository       $userRepository
     * @param \Procvic\Model\Repositories\UserEmailRepository  $userEmailRepository
     */
    public function __construct(UserRepository $userRepository, UserEmailRepository $userEmailRepository)
    {
        $this->userRepository = $userRepository;
        $this->userEmailRepository = $userEmailRepository;
    }


    /**
     * Try to authenticate user with provided credentials.
     *
     * @param  array $credentials                      Credentials provided by user who want to authenticate.
     * @throws \Nette\Security\AuthenticationException When wrong e-mail or password provided.
     * @todo   Error message translations.
     * @todo   Bring back implamentation of needsRehash.
     */
    public function authenticate(array $credentials)
    {
        list($email, $password) = $credentials;

        $user = $this->userRepository->getBy(['email' => $email]);

        if (!$user) {
            throw new AuthenticationException(
                "Zadán špatný e-mail",
                self::IDENTITY_NOT_FOUND
            );
        }

        if (!Passwords::verify($password, $user->password)) {
            throw new AuthenticationException(
                "Zadáno špatné heslo",
                self::INVALID_CREDENTIAL
            );
        }

        return new Identity($user->id);
    }
}
