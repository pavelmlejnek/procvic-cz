<?php

namespace Procvic\Model\Repositories;

/**
 * Class CategoryRepository
 *
 * @package    Procvic
 * @subpackage Model\Repositories
 * @SuppressWarnings(PHPMD)
 */
class CategoryRepository extends BaseRepository
{
    /**
     * @var string
     */
    public static $table = 'categories';

    /**
     * Return tree of categories for representation.
     *
     * `HAVING COUNT(e.exercise) > 6` is for fixed feature select last
     * unused exercise in SQL function `get_next_random_exercise_id`
     *
     * @param  int $categoryId Identifier for start listing of tree.
     * @return array           Tree of categories.
     */
    public function getTree($categoryId)
    {
        $items = [];
        $query = '
            SELECT c.*, COUNT(cat.exercise) as count_exercises
            FROM categories c
                LEFT JOIN exercises_categories cat ON cat.category = c.id
                LEFT JOIN exercises e ON e.id = cat.exercise
            WHERE e.authorize = 1
            GROUP BY c.id
            HAVING COUNT(cat.exercise) > 6
        ';
        $categories = $this->database->query($query)->fetchAll();

        foreach ($categories as $category) {
            if ($categoryId == $category->id) {
                $this->insertSubCategoriesForTree(
                    $items,
                    $categories,
                    $category->id,
                    0
                );
            }
        }

        return $items;
    }


    /**
     * Recursion generate subcategories for tree
     */
    private function insertSubCategoriesForTree(&$items, $categories, $parentId, $depth)
    {
        foreach ($categories as $category) {
            $isBaseCategory = $category->id == $parentId;

            if ($category->parent == $parentId && !$isBaseCategory) {
                $items[$category->id] = [
                    'item' => $category,
                    'depth' => $depth
                ];

                $this->insertSubCategoriesForTree(
                    $items,
                    $categories,
                    $category->id,
                    $depth + 1
                );
            }
        }
    }
}
