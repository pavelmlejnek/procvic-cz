<?php

namespace Procvic\Model\Repositories;

/**
 * Class ExerciseRepository
 *
 * @package    Procvic
 * @subpackage Model\Repositories
 */
class ExerciseRepository extends BaseRepository
{
    /**
     * @var string
     */
    public static $table = 'exercises';
}
