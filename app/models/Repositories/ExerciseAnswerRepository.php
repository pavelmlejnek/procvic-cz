<?php

namespace Procvic\Model\Repositories;

/**
 * Class ExerciseAnswerRepository
 *
 * @package    Procvic
 * @subpackage Model\Repositories
 */
class ExerciseAnswerRepository extends BaseRepository
{
    /**
     * @var string
     */
    public static $table = 'exercises_answers';
}
