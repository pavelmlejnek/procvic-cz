<?php

namespace Procvic\Model\Repositories;

/**
 * Class UserRepository
 *
 * @package    Procvic
 * @subpackage Model\Repositories
 * @SuppressWarnings(PHPMD)
 */
class UserRepository extends BaseRepository
{
    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';

    /**
     * @var string
     */
    public static $table = 'users';


    /**
     * @param string $email
     * @param array  $data
     */
    public function updateByEmail($email, array $data)
    {
        $this->table(self::$table)->where('email', $email)->update($data);
    }
}
