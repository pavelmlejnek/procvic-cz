<?php

namespace Procvic\Model\Repositories;

/**
 * Class UserEmailRepository
 *
 * @package    Procvic
 * @subpackage Model\Repositories
 */
class UserEmailRepository extends BaseRepository
{
    const UNVERIFIED = 0;
    const VERIFIED = 1;

    /**
     * @var string
     */
    public static $table = 'users_verified_emails';


    /**
     * @param  string $email
     * @return string
     */
    public function getHashByEmail($email)
    {
        return $this->table(self::$table)->select('hash')
            ->where([
                'user.email' => $email,
            ])->fetch()->hash;
    }


    /**
     * @param string $email
     * @param array  $data
     */
    public function updateByEmail($email, array $data)
    {
        $this->table(self::$table)
            ->where([
                'user' => $this->table(UserRepository::$table)->select('id')->where([
                    'email' => $email,
                ])
            ])->update($data);
    }


    /**
     * @param  string $email
     * @return boolean
     */
    public function isVerified($email)
    {
        return $this->table(self::$table)->select('verified')
            ->where([
                'user.email' => $email
            ])->fetch()->verified === 1;
    }

    /**
     * @param  string $email
     * @param  string $hash
     * @return boolean
     */
    public function isValidVerificationRequest($email, $hash)
    {
        return $this->table(self::$table)
            ->where([
                'hash' => $hash,
                'user.email' => $email,
            ])->count() === 1;
    }
}
