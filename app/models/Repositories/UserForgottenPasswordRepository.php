<?php

namespace Procvic\Model\Repositories;

/**
 * Class UserForgottenPasswordRepository
 *
 * @package    Procvic
 * @subpackage Model\Repositories
 */
class UserForgottenPasswordRepository extends BaseRepository
{
    /**
     * @var string
     */
    public static $table = 'users_forgotten_password';

    /**
     * @param  string $email
     * @param  string $hash
     * @return boolean
     */
    public function isValid($email, $hash)
    {
        $query = 'SELECT *
            FROM users_forgotten_password p
            WHERE
              p.hash = ? AND
              p.stamp >= DATE_SUB(NOW(), INTERVAL 2 HOUR) AND
              p.user = (SELECT u.id FROM users u WHERE u.email = ?)';

        $user = $this->database->query($query, $hash, $email)->fetch();

        return !empty($user);
    }


    /**
     * @param string $email
     */
    public function deleteByEmail($email)
    {
        $this->table(self::$table)
            ->where([
                'user' => $this->table(UserRepository::$table)->select('id')->where([
                    'email' => $email,
                ])
            ])->delete();
    }
}
