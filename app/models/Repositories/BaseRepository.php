<?php

namespace Procvic\Model\Repositories;

use Nette\Object;
use Nette\Database\Context;

/**
* @package    Procvic
* @subpackage Model\Repositories
* @SuppressWarnings(PHPMD)
*/
abstract class BaseRepository extends Object
{
    /**
     * @var \Nette\Database\Context
     */
    protected $database;


    /**
     * @var string
     */
    public static $table;


    /**
     * @param \Nette\Database\Context $database
     */
    public function __construct(Context $database)
    {
        $this->database = $database;
    }


    /**
     * Return all valid entries from the database.
     *
     * @return \Nette\Database\Table\Selection
     */
    public function findAll()
    {
        $class = get_called_class();

        return $this->table($class::$table);
    }


    /**
     * @param  string $table
     * @return \Nette\Database\Table\Selection
     */
    public function table($table)
    {
        return $this->database->table($table);
    }


    /**
     * Return a collection of entries based on condition.
     *
     * @param  array $where
     * @return \Nette\Database\Table\Selection
     */
    public function findBy($where)
    {
        return $this->findAll()->where($where);
    }


    /**
     * Return one entry by identification key.
     *
     * @param  integer $id
     * @return \Nette\Database\Table\ActiveRow|false
     */
    public function get($id)
    {
        return $this->findAll()->get($id);
    }


    /**
     * Return one entry base on condition.
     *
     * @param  array
     * @return \Nette\Database\Table\ActiveRow|false
     */
    public function getBy($where)
    {
        return $this->findAll()->where($where)->fetch();
    }


    /**
     * Insert a new entry into the database.
     *
     * @param array $data
     * @return \Nette\Database\Table\IRow|integer
     */
    public function insert($data)
    {
        return $this->findAll()->insert($data);
    }


    /**
     * Delete one entry from the database by identification key.
     *
     * @param integer $id
     */
    public function delete($id)
    {
        $this->findAll()->delete($id);
    }


    /**
     * Update one entry from database by identification key.
     *
     * @param integer $id
     */
    public function update($id, array $data)
    {
        return $this->findAll()->where('id', $id)->update($data);
    }
}
