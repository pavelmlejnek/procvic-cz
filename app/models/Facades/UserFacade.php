<?php

namespace Procvic\Model\Facades;

use Nette\Object;
use Nette\Utils\ArrayHash;
use Nette\Security\Passwords;
use Procvic\Model\Repositories\UserEmailRepository;
use Procvic\Model\Repositories\UserForgottenPasswordRepository;
use Procvic\Model\Repositories\UserRepository;

/**
 * Manager class for working with users.
 *
 * This class covers all operations with user. Handles authentication
 * and also adding and deleting.
 *
 * @package Procvic
 * @subpackage Model
 * @SuppressWarnings(PHPMD)
 */
class UserFacade extends Object
{
    /**
     * @var array
     */
    public $onForgottenPasswordRequest = [];

    /**
     * @var array
     */
    public $onRegistrationWithPasswordCompleted = [];

    /**
     * @var array
     */
    public $onResendVerificationEmail = [];

    /**
     * @var \Procvic\Model\Repositories\UserRepository
     */
    private $userRepository;

    /**
     * @var \Procvic\Model\Repositories\UserEmailRepository
     */
    private $userEmailRepository;

    /**
     * @var \Procvic\Model\Repositories\UserForgottenPasswordRepository
     */
    private $userForgottenPasswordRepository;


    /**
     * @param \Procvic\Model\Repositories\UserRepository                  $userRepository
     * @param \Procvic\Model\Repositories\UserEmailRepository             $userEmailRepository
     * @param \Procvic\Model\Repositories\UserForgottenPasswordRepository $userForgottenPasswordRepository
     */
    public function __construct(
        UserRepository $userRepository,
        UserEmailRepository $userEmailRepository,
        UserForgottenPasswordRepository $userForgottenPasswordRepository
    ) {
        $this->userRepository = $userRepository;
        $this->userEmailRepository = $userEmailRepository;
        $this->userForgottenPasswordRepository = $userForgottenPasswordRepository;
    }


    /**
     * @param integer $userId
     */
    public function delete($userId)
    {
        $this->userRepository->delete($userId);
    }


    /**
     * @param $email
     * @return false|\Nette\Database\Table\ActiveRow
     */
    public function getByEmail($email)
    {
        return $this->userRepository->getBy(['email' => $email]);
    }


    /**
     * @param $userId
     * @return false|\Nette\Database\Table\ActiveRow
     */
    public function getById($userId)
    {
        return $this->userRepository->getBy(['id' => $userId]);
    }


    /**
     * @param $facebookId
     * @return false|\Nette\Database\Table\ActiveRow
     */
    public function getByFacebookId($facebookId)
    {
        return $this->userRepository->getBy(['facebook_id' => $facebookId]);
    }


    /**
     * @param  string $email
     * @return boolean
     */
    public function isFacebookUser($email)
    {
        $user = $this->getByEmail($email);

        return $user->facebook_id ? true : false;
    }


    /**
     * @param  string $email
     * @param  string $password
     * @return int|\Nette\Database\Table\IRow
     */
    public function registerWithPassword($email, $password)
    {
        $user = $this->userRepository->insert([
            'email' => $email,
            'password' => Passwords::hash($password),
            'role' => UserRepository::ROLE_USER,
        ]);

        $hash = sha1($email . time());

        $this->userEmailRepository->insert([
            'user' => $user->id,
            'hash' => $hash,
            'verified' => UserEmailRepository::UNVERIFIED
        ]);

        // fire event
        $this->onRegistrationWithPasswordCompleted($email, $hash);

        return $user;
    }


    /**
     * @param  string                 $facebookId
     * @param  \Nette\Utils\ArrayHash $facebookProfile
     * @return integer|\Nette\Database\Table\IRow
     */
    public function registerWithFacebook($facebookId, ArrayHash $facebookProfile)
    {
        $user = $this->userRepository->insert([
            'facebook_id' => $facebookId,
            'email' => $facebookProfile->email,
            'name' => $facebookProfile->first_name,
            'surname' => $facebookProfile->last_name,
            'role' => UserRepository::ROLE_USER
        ]);

        $this->userEmailRepository->insert([
            'user' => $user->id,
            'verified' => UserEmailRepository::VERIFIED
        ]);

        return $user;
    }


    /**
     * @param  string                 $email
     * @param  \Nette\Utils\ArrayHash $facebookProfile
     * @return false|\Nette\Database\Table\ActiveRow
     */
    public function mergeWithFacebook($email, ArrayHash $facebookProfile)
    {
        $this->userRepository->updateByEmail($email, [
            'facebook_id' => $facebookProfile->id, // this marks the account as facebook one
            'name' => $facebookProfile->first_name,
            'surname' => $facebookProfile->last_name
        ]);

        return $this->getByEmail($email);
    }


    /**
     * @param integer $userId
     * @param string  $facebookAccessToken
     */
    public function updateFacebookAccessToken($userId, $facebookAccessToken)
    {
        $this->userRepository->update($userId, ['facebook_access_token' => $facebookAccessToken]);
    }


    /**
     * @param integer $userId
     * @param string  $name
     */
    public function updateName($userId, $name)
    {
        $this->userRepository->update($userId, ['name' => $name]);
    }


    /**
     * @param integer $userId
     * @param string  $surname
     */
    public function updateSurname($userId, $surname)
    {
        $this->userRepository->update($userId, ['surname' => $surname]);
    }


    /**
     * @param integer $userId
     * @param string  $about
     */
    public function updateAbout($userId, $about)
    {
        $this->userRepository->update($userId, ['about' => $about]);
    }


    /**
     * @param  string $email
     * @return boolean
     */
    public function isEmailVerified($email)
    {
        return $this->userEmailRepository->isVerified($email);
    }


    /**
     * @param string $email
     * @param string $hash
     */
    public function isValidEmailVerification($email, $hash)
    {
        return $this->userEmailRepository->isValidVerificationRequest($email, $hash);
    }


    /**
     * @param string $email
     */
    public function verify($email)
    {
        $this->userEmailRepository->updateByEmail($email, ['verified' => UserEmailRepository::VERIFIED]);
    }


    /**
     * @param string $email
     */
    public function resendVerificationEmail($email)
    {
        $hash = $this->userEmailRepository->getHashByEmail($email);

        //fire event
        $this->onResendVerificationEmail($email, $hash);
    }


    /**
     * @param $email
     */
    public function requestNewPassword($email)
    {
        if (!$user =  $this->userRepository->getBy(['email' => $email])) {
            throw new \InvalidArgumentException("User with e-mail '$email' not found.");
        }

        $hash = sha1($email . time());

        $this->userForgottenPasswordRepository->insert([
            'user' => $user->id,
            'hash' => $hash,
        ]);

        // fire event
        $this->onForgottenPasswordRequest($email, $hash);
    }


    /**
     * @param string $email
     * @param string $hash
     * @return boolean
     */
    public function isValidNewPasswordRequest($email, $hash)
    {
        return $this->userForgottenPasswordRepository->isValid($email, $hash);
    }


    /**
     * Check whether provided password is real user's password
     *
     * @param  string $email
     * @param  string $password
     * @return boolean
     */
    public function isValidPassword($email, $password)
    {
        $user = $this->getByEmail($email);

        if (Passwords::verify($password, $user->password)) {
            return true;
        }

        return false;
    }


    /**
     * @param string $email
     * @param string $password
     */
    public function changePassword($email, $password)
    {
        $this->userRepository->updateByEmail($email, ['password' => Passwords::hash($password)]);
    }


    /**
     * Change password and remove request
     *
     * @param string $email
     * @param string $password
     */
    public function changeForgottenPassword($email, $password)
    {
        $this->changePassword($email, $password);
        $this->userForgottenPasswordRepository->deleteByEmail($email);
    }


    /**
     * @param \Procvic\Model\Identity $identity
     */
    public function loadIdentity($identity)
    {
        $user = $this->getById($identity->getId());

        $identity->facebookId = $user->facebook_id;
        $identity->facebookAccessToken = $user->facebook_access_token;
        $identity->email = $user->email;
        $identity->role = $user->role;
        $identity->name = $user->name;
        $identity->surname = $user->surname;
        $identity->about = $user->about;
        $identity->createdAt = $user->created_at;
        $identity->verified = $this->isEmailVerified($user->email);

        $identity->upToDate = true;
    }
}
