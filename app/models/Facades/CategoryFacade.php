<?php

namespace Procvic\Model\Facades;

use Kdyby;
use Nette\Object;
use Procvic\Model\Repositories\CategoryRepository;
use Procvic\Model\Entities\Category;

/**
 * Category facade
 *
 * @package Procvic
 * @subpackage Model\Facades
 */
class CategoryFacade extends Object
{
    /**
     * @var \Procvic\Model\Repositories\CategoryRepository
     */
    private $categoryRepository;


    /**
     * @param \Procvic\Model\Repositories\CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }


    /**
     * @SuppressWarnings(PHPMD)
     */
    public function getById($id)
    {
        return Category::fromActiveRow($this->categoryRepository->getBy(['id' => $id]));
    }


    /**
     * Generate array of all categories in format id => name
     *
     * @return array
     */
    public function getCategoriesForSelect()
    {
        $items = [];
        $categories = $this->categoryRepository->findAll()->fetchAll();

        foreach ($categories as $category) {
            if ($category->id == $category->parent) {
                $items[$category->id] = $category->name;

                $this->insertSubCategoriesForSelect(
                    $items,
                    $categories,
                    $category->id,
                    $category->name
                );
            }
        }

        return $items;
    }


    /**
     * Recursion generate subcategories
     */
    private function insertSubCategoriesForSelect(&$items, $categories, $parentId, $baseName)
    {
        foreach ($categories as $category) {
            $isBaseCategory = $category->id == $parentId;

            if ($category->parent == $parentId && !$isBaseCategory) {
                $name = $baseName . ' / ' . $category->name;
                $items[$category->id] = $name;

                $this->insertSubCategoriesForSelect(
                    $items,
                    $categories,
                    $category->id,
                    $name
                );
            }
        }
    }


    /**
     * @param  integer $categoryId
     * @return array
     */
    public function getTree($categoryId)
    {
        return $this->categoryRepository->getTree($categoryId);
    }
}
