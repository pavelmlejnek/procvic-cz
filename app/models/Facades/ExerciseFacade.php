<?php

namespace Procvic\Model\Facades;

use Kdyby;
use Nette\Object;
use Procvic\Model\Repositories\ExerciseRepository;
use Procvic\Model\Entities\Exercise;

/**
 * Exercise facade
 *
 * @package    Procvic
 * @subpackage Model\Facades
 */
class ExerciseFacade extends Object
{
    /**
     * @var \Procvic\Model\Repositories\ExerciseRepository
     */
    private $exerciseRepository;

    /**
     * @param \Procvic\Model\Repositories\ExerciseRepository $exerciseRepository
     */
    public function __construct(ExerciseRepository $exerciseRepository)
    {
        $this->exerciseRepository = $exerciseRepository;
    }


    /**
     * @param integer $id
     * @return static
     * @SuppressWarnings(PHPMD)
     */
    public function getById($id)
    {
        return Exercise::fromActiveRow($this->exerciseRepository->getBy(['id' => $id]));
    }
}
