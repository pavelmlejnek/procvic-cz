<?php

namespace Procvic\Model;

use Nette\Object;
use Nette\Database\Connection;

/**
 * Model class for random exercise.
 *
 * @package Procvic
 * @subpackage Model
 */
class RandomExerciseModel extends Object
{
    /**
     * @var \Nette\Database\Connection
     */
    private $database;


    /**
     * @param \Nette\Database\Connection $database
     */
    public function __construct(Connection $database)
    {
        $this->database = $database;
    }


    /**
     * Get random exercise
     *
     * @param int $categoryID
     * @param int $userID if is $userID == NULL then isn't user log in
     * @return array
     */
    public function get($categoryID, $userID)
    {
        if ($userID == null) {
            // user is not log in
            $query = 'SELECT get_next_random_exercise_id_for_anonymous(?)';
            $exerciseID = $this->database->query(
                $query,
                $categoryID
            )->fetchField();
        } else {
            // user is log in
            $query = 'SELECT get_next_random_exercise_id(?, ?)';
            $exerciseID = $this->database->query(
                $query,
                $categoryID,
                $userID
            )->fetchField();
        }

        $query = 'SELECT * FROM exercises WHERE id = ?';
        $exercise = $this->database->query($query, $exerciseID)->fetch();

        $query = 'SELECT * FROM exercises_answers WHERE exercise = ? ORDER BY RAND()';
        $answers = $this->database->query($query, $exerciseID)->fetchAll();

        return [
            'question' => $exercise,
            'answers' => $answers,
        ];
    }


    /**
     * Save user answer
     *
     * @param int $userID
     * @param int $answerID
     * @param int $categoryID
     */
    public function saveUserAnswer($userID, $answerID, $categoryID)
    {
        $query = 'INSERT INTO exercises_users_answers (answer, category, user)
            VALUES (?, ?, ?)';
        $this->database->query($query, $answerID, $categoryID, $userID);
    }
}
