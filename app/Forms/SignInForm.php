<?php

namespace Procvic\ApplicationModule\Forms;

use Nette\Object;
use Nette\Application\UI\Form;

/**
 * @package    Procvic
 * @subpackage ApplicationModule\Forms
 */
class SignInForm extends Object
{
    /**
     * @return \Nette\Application\UI\Form
     */
    public function create()
    {
        $form = new Form();

        $form->addText('email')
            ->setRequired('sign.in.please_enter_email');

        $form->addPassword('password')
            ->setRequired('sign.in.please_enter_password');

        $form->addCheckbox('remember');

        $form->addSubmit('send', 'sign.in.action_button');

        return $form;
    }
}
