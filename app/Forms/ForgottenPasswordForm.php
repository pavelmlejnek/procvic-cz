<?php

namespace Procvic\ApplicationModule\Forms;


use Nette;
use Nette\Object;
use Nette\Application\UI\Form;
use Procvic\ApplicationModule\Model\Managers\UserManager;

/**
 * @package    Procvic
 * @subpackage ApplicationModule\Forms
 */
class ForgottenPasswordForm extends Object
{
    /**
     * @var \Procvic\Model\UserManager
     */
    private $userManager;


    /**
     * @param \Procvic\Model\UserManager $userManager
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }


    /**
     * @return \Nette\Application\UI\Form
     */
    public function create()
    {
        $form = new Form();

        $form->addText('email', 'frontend-forgottenPassword.default.form_email')
            ->addRule(Form::EMAIL, 'frontend-forgottenPassword.default.form_validation_email_has_bad_format')
            ->setRequired('frontend-forgottenPassword.default.form_validation_email_is_required');

        $form->addSubmit('send', 'frontend-forgottenPassword.default.form_action_button');

        $form->onSuccess[] = $this->forgottenPasswordFormSucceeded;

        return $form;
    }


    /**
     * @param \Nette\Application\UI\Form $form
     * @param \Nette\Utils\ArrayHash     $values
     */
    public function forgottenPasswordFormSucceeded($form, $values)
    {
        try {
            $this->userManager->addChangeForgottenPasswordRequest($values->email);
        } catch (\InvalidArgumentException $e) {
            $form->addError('frontend-forgottenPassword.default.form_validation_email_does_not_exist');
        }
    }
}
