<?php

namespace Procvic\ApplicationModule\Forms;

use Nette;
use Nette\Object;
use Nette\Application\UI\Form;
use Procvic\ApplicationModule\Model\Managers\UserManager;

/**
 * @package    Procvic
 * @subpackage ApplicationModule\Forms
 */
class ChangePasswordForm extends Object
{
    /**
     * @var \Procvic\Model\UserManager
     */
    private $userManager;


    /**
     * @param \Procvic\Model\UserManager $userManager
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }


    /**
     * @param string $email E-mail of user who is changing his password.
     */
    public function create($email)
    {
        $changePasswordForm = new Form();

        $changePasswordForm->addHidden('email', $email);

        $changePasswordForm->addPassword('password', 'frontend-forgottenPassword.change.form_password')
            ->addRule(Form::MIN_LENGTH, 'frontend-forgottenPassword.change.form_validation_password_is_too_short', 3)
            ->setRequired('frontend-forgottenPassword.change.form_validation_password_is_required');

        $changePasswordForm->addPassword('password_repeat', 'frontend-forgottenPassword.change.form_password_repeat')
            ->addRule(
                Form::EQUAL,
                'frontend-forgottenPassword.change.form_password_repeat_must_be_equal_to_password',
                $changePasswordForm['password']
            )
            ->setRequired('');

        $changePasswordForm->addSubmit('send', 'frontend-forgottenPassword.change.form_action_button');

        $changePasswordForm->onSuccess[] = $this->changePasswordFormSucceeded;

        return $changePasswordForm;
    }


    /**
     * @param \Nette\Application\UI\Form $form
     */
    public function changePasswordFormSucceeded($form)
    {
        $values = $form->getValues();

        $this->userManager->changePassword($values->email, $values->password);
    }
}
