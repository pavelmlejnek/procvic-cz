<?php

namespace Procvic\Router;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\SimpleRouter;

/**
 * Router factory
 *
 * @package Procvic
 * @subpackage Router
 */
class RouterFactory
{
    /**
     * @return \Nette\Application\IRouter
     */
    public function createRouter()
    {
        $router = new RouteList();

        $router[] = $wikiRouter = new RouteList('Wiki');

        $wikiRouter[] = new Route('wiki/<slug>', 'Page:show');
        $wikiRouter[] = new Route('wiki/<slug>/create', 'Page:create');
        $wikiRouter[] = new Route('wiki/<slug>/edit', 'Page:edit');
        $wikiRouter[] = new Route('wiki/<slug>/revisions', 'Page:revisions');
        $wikiRouter[] = new Route('wiki/<presenter>/<action>[/<id>]');

        $router[] = new Route('[<locale=cs cs|en>/]<presenter>/<action>[/<id>]', 'Homepage:default');

        return $router;
    }
}
