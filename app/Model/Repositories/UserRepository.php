<?php

namespace Procvic\ApplicationModule\Model\Repositories;

use Nette\Object;
use Nette\Database\Context;
use Nette\Security\Passwords;
use Nette\Utils\ArrayHash;

/**
 * Model class for users.
 *
 * @package Procvic
 * @subpackage ApplicationModule\Model\Repositories
 * @SuppressWarnings(PHPMD)
 */
class UserRepository extends Object
{
    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';


    /**
     * @var \Nette\Database\Connection
     */
    private $database;


    /**
     * @param \Nette\Database\Connection $database
     */
    public function __construct(Context $database)
    {
        $this->database = $database;
    }


    /**
     * @return array
     */
    public function findAll()
    {
        $query = 'SELECT * FROM users';

        return $this->database->query($query)->fetchAll();
    }


    /**
     * @param  string $email
     * @return \Nette\Database\Row
     */
    public function getByEmail($email)
    {
        $query = 'SELECT * FROM users
            WHERE email = ?';

        return $this->database->query($query, $email)->fetch();
    }


    /**
     * @param  integer $id
     * @return \Nette\Database\Row
     */
    public function getById($id)
    {
        $query = 'SELECT *
            FROM users WHERE id = ?';

        return $this->database->query($query, $id)->fetch();
    }


    /**
     * Insert user to the database.
     *
     * @todo The rest of columns (could be null in ArrayHash)
     * @param \Nette\Utils\ArrayHash $data
     */
    public function insert(ArrayHash $data)
    {
        $query = 'INSERT INTO users (email, password, role, created_at)
            VALUES (?, ?, ?, NOW())';

        return $this->database->query(
            $query,
            $data->email,
            $data->password,
            self::ROLE_USER
        );
    }


    /**
     * @param integer $id
     */
    public function delete($id)
    {
        $query = 'DELETE FROM users
            WHERE id = ?';

        return $this->database->query($query, $id);
    }


    /**
     * @param string $email
     */
    public function addChangeForgottenPasswordRequest($email, $hash)
    {
        $query = 'INSERT INTO users_forgotten_password (user, stamp, hash)
            VALUES ((SELECT id FROM users WHERE email = ?), NOW(), ?)';

        return $this->database->query($query, $email, $hash);
    }


    /**
     * @param string $email
     * @param string $hash
     */
    public function isValidChangeForgottenPasswordRequest($email, $hash)
    {
        $query = 'SELECT *
            FROM users_forgotten_password p
            WHERE
              p.hash = ? AND
              p.stamp >= DATE_SUB(NOW(), INTERVAL 2 HOUR) AND
              p.user = (SELECT u.id FROM users u WHERE u.email = ?)';

        $user = $this->database->query($query, $hash, $email)->fetch();

        return !empty($user);
    }


    /**
     * @param string $email
     */
    public function isEmailVerified($email)
    {
        $query = 'SELECT verified FROM users_verified_emails e
            WHERE e.user = (SELECT u.id FROM users u WHERE u.email = ?)';

        $emailVerified = $this->database->query($query, $email)->fetch();

        return $emailVerified->verified == 1 ? true : false;
    }


    /**
     * @param string $email
     */
    public function verifyEmail($email)
    {
        $query = 'UPDATE users_verified_emails e
            SET e.verified = 1 WHERE e.user = (SELECT u.id FROM users u WHERE u.email = ?)';

        return $this->database->query($query, $email);
    }


    /**
     * @param string $email
     * @param string $hash
     */
    public function addUnverifiedEmail($email, $hash)
    {
        $query = 'INSERT INTO users_verified_emails (hash, user)
            VALUES (?, (SELECT id FROM users WHERE email = ?))';

        return $this->database->query($query, $hash, $email);
    }


    /**
     * @param  string $email
     * @param  string $hash
     * @return boolean
     */
    public function isValidEmailVerification($email, $hash)
    {
        $query = 'SELECT id FROM users_verified_emails e
            WHERE e.hash = ? AND
                e.user = (SELECT u.id FROM users u WHERE u.email = ?)';

        $verifiedEmail = $this->database->query($query, $hash, $email);

        return !empty($verifiedEmail);
    }


    /**
     * @param  string $email
     * @return string
     */
    public function getVerificationHashByEmail($email)
    {
        $query = 'SELECT hash FROM users_verified_emails e
            WHERE e.user = (SELECT u.id FROM users u WHERE u.email = ?)';

        $result = $this->database->query($query, $email)->fetch();

        return $result->hash;
    }


    /**
     * @param string $email
     * @param string $password
     */
    public function changePassword($email, $password)
    {
        $query = 'UPDATE users SET password = ?
            WHERE email = ?';

        return $this->database->query($query, $password, $email);
    }


    public function addUser($email, $password, $role, $name, $surname)
    {
        $query = 'INSERT INTO users (email, password, name, surname, role, created_at)
            VALUES (?, ?, ?, ?, ?, NOW())';

        return $this->database->query($query, $email, $password, $name, $surname, $role);
    }


    /**
     * @param integer $id
     * @param string  $email
     */
    public function updateEmail($id, $email)
    {
        $query = 'UPDATE users SET email = ? WHERE id = ?';

        return $this->database->query($query, $email, $id);
    }


    /**
     * @param integer $id
     * @param string  $name
     */
    public function updateName($id, $name)
    {
        $query = 'UPDATE users SET name = ? WHERE id = ?';

        return $this->database->query($query, $name, $id);
    }


    /**
     * @param integer $id
     * @param string  $surname
     */
    public function updateSurname($id, $surname)
    {
        $query = 'UPDATE users SET surname = ? WHERE id = ?';

        return $this->database->query($query, $surname, $id);
    }


    /**
     * @param integer $id
     * @param string  $role
     */
    public function updateRole($id, $role)
    {
        $query = 'UPDATE users SET role = ? WHERE id = ?';

        return $this->database->query($query, $role, $id);
    }


    /**
     * @param integer $id
     * @param string  $password
     */
    public function updatePassword($id, $password)
    {
        $query = 'UPDATE users SET password = ? WHERE id = ?';

        return $this->database->query($query, $password, $id);
    }
}
