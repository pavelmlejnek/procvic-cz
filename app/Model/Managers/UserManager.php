<?php

namespace Procvic\ApplicationModule\Model\Managers;

use Nette\Object;
use Nette\Utils\ArrayHash;
use Nette\Security\IAuthenticator;
use Nette\Security\AuthenticationException;
use Nette\Security\Passwords;
use Nette\Security\Identity;
use Procvic\Model\Facades\UserFacade;
use Procvic\ApplicationModule\Model\Repositories\UserRepository;

/**
 * Manager class for working with users.
 *
 * This class covers all operations with user. Handles authentication
 * and also adding and deleting.
 *
 * @package Procvic
 * @subpackage Model
 * @SuppressWarnings(PHPMD)
 */
class UserManager extends Object implements IAuthenticator
{
    /**
     * @var array
     */
    public $onForgottenPasswordRequest = [];

    /**
     * @var array
     */
    public $onRegistrationCompleted = [];

    public $onResendVerificationEmail = [];

    /**
     * @var \Procvic\Model\Repositories\UserRepository
     */
    private $userRepository;


    /**
     * @param \Procvic\Model\Repositories\UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    /**
     * Try to authenticate user with provided credentials.
     *
     * @param  array $credentials                      Credentials provided by user who want to authenticate.
     * @throws \Nette\Security\AuthenticationException When wrong e-mail or password provided.
     * @return \FinWatch\Model\Entities\UserEntity     User represented by entity with null password.
     * @todo   Error message translations.
     * @todo   Bring back implamentation of needsRehash.
     */
    public function authenticate(array $credentials)
    {
        list($email, $password) = $credentials;

        $user = $this->userRepository->getByEmail($email);

        if (!$user) {
            throw new AuthenticationException(
                "Zadán špatný e-mail",
                self::IDENTITY_NOT_FOUND
            );
        }

        if (!Passwords::verify($password, $user->password)) {
            throw new AuthenticationException(
                "Zadáno špatné heslo",
                self::INVALID_CREDENTIAL
            );
        }

        return new Identity($user->id, $user->role, [
            'email' => $user->email,
            'name' => $user->name,
            'surname' => $user->surname,
            'verified' => $this->userRepository->isEmailVerified($email)
        ]);
    }


    /**
     * Delete given user represented by identification number.
     *
     * <code>
     *     $userManager->deleteUser(1); // deletes user with id 1
     * </code>
     *
     * @param  int $userId Identification Number of user who should be deleted.
     * @throws \InvalidArgumentException  When user wasn't found.
     */
    public function deleteUser($userId)
    {
        $this->userRepository->delete($userId);
    }


    /**
     * @param string $email
     * @param string $password
     */
    public function registerUser($email, $password)
    {
        $credentials = ArrayHash::from([
            'email' => $email,
            'password' => Passwords::hash($password)
        ]);
        $this->userRepository->insert($credentials);

        $hash = sha1($email . time());

        $this->userRepository->addUnverifiedEmail($email, $hash);

        // fire event
        $this->onRegistrationCompleted($email, $hash);
    }


    public function addUser($email, $password, $role = 'user', $name = null, $surname = null)
    {
        $this->userRepository->addUser($email, $password, $role, $name, $surname);
    }


    /**
     * @param  string $email
     * @return boolean
     */
    public function isEmailVerified($email)
    {
        return $this->userRepository->isEmailVerified($email);
    }


    /**
     * @param string $email
     * @param string $hash
     */
    public function isValidEmailVerification($email, $hash)
    {
        return $this->userRepository->isValidEmailVerification($email, $hash);
    }


    /**
     * @param string $email
     */
    public function verifyEmail($email)
    {
        $this->userRepository->verifyEmail($email);
    }


    /**
     * @param string $email
     */
    public function resendVerificationEmail($email)
    {
        $hash = $this->userRepository->getVerificationHashByEmail($email);

        //fire event
        $this->onResendVerificationEmail($email, $hash);
    }


    /**
     * @param string $email
     * @todo  To save some perfomance there is a possibility of new method in user
     *        repository called for example 'checkIfUserExists' which grabs only id from database.
     */
    public function addChangeForgottenPasswordRequest($email)
    {
        if (!$this->userRepository->getByEmail($email)) {
            throw new \InvalidArgumentException("User with e-mail '$email' not found.");
        }

        $hash = sha1($email . time());

        $this->userRepository->addChangeForgottenPasswordRequest($email, $hash);

        // fire event
        $this->onForgottenPasswordRequest($email, $hash);
    }


    /**
     * @param string $email
     * @param string $hash
     * @return boolean
     */
    public function isValidChangeForgottenPasswordRequest($email, $hash)
    {
        return $this->userRepository
            ->isValidChangeForgottenPasswordRequest($email, $hash);
    }


    /**
     * @param string $email
     * @param string $password
     */
    public function changePassword($email, $password)
    {
        $this->userRepository->changePassword($email, Passwords::hash($password));
    }
}
