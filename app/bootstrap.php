<?php

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/helpers/Configurator.php';

$configurator = new Procvic\Helpers\Configurator;
$configurator->enableCompression();

$configurator->enableDebugger(__DIR__ . '/../log');
$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
    ->addDirectory(__DIR__)
    ->addDirectory(__DIR__ . '/../vendor/others')
    ->register();

$configurator->addConfig(__DIR__ . '/config/config.neon', Nette\Configurator::AUTO);

$container = $configurator->createContainer();

return $container;
