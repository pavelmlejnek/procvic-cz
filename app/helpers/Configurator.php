<?php

namespace Procvic\Helpers;

class Configurator extends \Nette\Configurator
{
    /**
     * Detects debug mode by IP address.
     * @SuppressWarnings(PHPMD)
     * @param  array  IP addresseswhitelist detection
     * @return bool
     */
    public static function detectDebugMode($list = null)
    {
        $list[] = '127.0.0.1';
        $list[] = '::1';
        $list[] = '192.168.33.1';

        return in_array($_SERVER['REMOTE_ADDR'], $list, true);
    }


    protected function getDefaultParameters()
    {
        $args = parent::getDefaultParameters();
        $args['appDir'] = __DIR__ . '/..';
        $args['wwwDir'] = __DIR__ . '/../../www';
        $args['environment'] = $this->getEnvironment();

        return $args;
    }


    /**
     * @SuppressWarnings(PHPMD)
     */
    private function getEnvironment()
    {
        if ($_SERVER['SERVER_NAME'] == 'beta.app.procvic.cz') {
            return 'beta';
        }
        if (self::detectDebugMode()) {
            return 'development';
        }
        return 'production';
    }


    public function enableCompression()
    {
        // test
        $allowed = ob_gzhandler('', \PHP_OUTPUT_HANDLER_START);
        if ($allowed === false) {
            return false; // not allowed
        }

        if (function_exists('ini_set')) {
            ini_set('zlib.output_compression', 'Off');
            ini_set('zlib.output_compression_level', '6');
        }
        ob_start('ob_gzhandler');
        return true;
    }
}
