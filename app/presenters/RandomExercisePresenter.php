<?php

namespace Procvic\Presenters;

/**
 * New random exercises presenter - for testing exercies
 *
 * @package    Procvic
 * @subpackage Presenters
 */
final class RandomExercisePresenter extends BasePresenter
{
    /**
     * @var \Procvic\Model\RandomExerciseModel
     * @inject
     */
    public $modelRandomExercise;

    /**
     * @var \Procvic\Model\ExerciseModel
     * @inject
     */
    public $modelExercise;

    private $exercise;


    public function renderDefault($categoryID)
    {
        $userID = $this->user->getId();
        $this->exercise = $this->modelRandomExercise->get($categoryID, $userID);
        $this->template->exercise = $this->exercise;
        $this->template->category = $categoryID;
    }


    public function actionCheck($exerciseID, $answerID, $categoryID)
    {
        $userID = $this->user->getId();
        $this->exercise = $this->modelExercise->get($exerciseID);
        $this->template->exercise = $this->exercise;
        $this->template->categoryID = $categoryID;
        $this->template->answerID = $answerID;

        // log answer to database
        if ($this->user->isLoggedIn()) {
            $this->modelRandomExercise->saveUserAnswer($userID, $answerID, $categoryID);
        }
    }
}
