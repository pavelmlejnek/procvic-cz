<?php

namespace Procvic\Presenters;

use Nette;

/**
 * Category presenter
 *
 * @package    Procvic
 * @subpackage Presenters
 */
final class CategoryPresenter extends BasePresenter
{
    /**
     * @var \Procvic\Model\Facades\CategoryFacade
     * @inject
     */
    public $categoryFacade;


    /**
     * @SuppressWarnings(PHPMD)
     */
    public function renderDefault($id)
    {
        $category = $this->categoryFacade->getById($id);

        $this->template->title = $category->name;
        $this->template->categories = $this->categoryFacade->getTree($id);
    }
}
