<?php

namespace Procvic\Presenters;

use Nette;

/**
 * Homepage presenter
 *
 * @package    Procvic
 * @subpackage Presenters
 */
final class HomepagePresenter extends BasePresenter
{
}
