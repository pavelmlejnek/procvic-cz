<?php

namespace Procvic\Presenters;

use Nette;

/**
 * Error presenter.
 */
final class ErrorPresenter extends BasePresenter
{
    /**
     * @param Exception
     */
    public function renderDefault($exception)
    {
        if ($exception instanceof Nette\Application\BadRequestException) {
            $code = $exception->getCode();
            // load template 403.latte or 404.latte or ... 4xx.latte
            $this->setView(in_array($code, [403, 404, 405, 410, 500]) ? $code : '4xx');
            // log to access.log
            $this->logger->log(
                "HTTP code $code: {$exception->getMessage()} in {$exception->getFile()}:{$exception->getLine()}",
                'access'
            );
        } else {
            $this->setView('500'); // load template 500.latte
            $this->logger->log($exception, ILogger::EXCEPTION); // and log exception
        }

        if ($this->isAjax()) {
            $this->payload->error = true;
            $this->terminate();
        }
    }
}
