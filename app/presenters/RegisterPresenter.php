<?php

namespace Procvic\Presenters;

use Nette;
use Procvic\Traits\TRedirectSignedUser;

/**
 * Register presenter
 *
 * @package    Procvic
 * @subpackage Presenters
 */
final class RegisterPresenter extends BasePresenter
{
    use TRedirectSignedUser;


    /**
     * @var \Procvic\Model\Facades\UserFacade
     * @inject
     */
    public $userFacade;


    /**
     * @var \Procvic\Components\SignControl\ISignControlFactory
     * @inject
     */
    public $signControlFactory;


    /**
     * @param string $email
     * @param string $hash
     */
    public function actionVerifyEmail($email, $hash)
    {
        if ($this->userFacade->isEmailVerified($email)) {
            $this->flashMessage('Tento e-mail je již ověřen.', 'info');
            $this->redirect(':Homepage:');
        }

        if (!$this->userFacade->isValidEmailVerification($email, $hash)) {
            $this->flashMessage('Neplatný pokus o ověření e-mailu.', 'alert');
            $this->redirect(':Homepage:');
        }

        $this->userFacade->verify($email);

        $this->flashMessage('E-mail byl úspěšně ověřen.', 'success');
        $this->redirect(':Homepage:');
    }


    /**
     * Check whether e-mail is verified and if not, resend verification e-mail.
     *
     * @param string $email
     */
    public function actionResendVerificationEmail($email)
    {
        if ($this->userFacade->isEmailVerified($email)) {
            $this->flashMessage('Tento e-mail je již ověřen.', 'alert');
            $this->redirect(':Homepage:');
        }

        $this->userFacade->resendVerificationEmail($email);

        $this->flashMessage('Byl znovu odeslán ověřovací e-mail.', 'success');
        $this->redirect(':Homepage:');
    }
}
