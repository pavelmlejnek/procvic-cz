<?php

namespace Procvic\Presenters;

use Nette\Application\UI\Form;

/**
 * New exercises presenter
 *
 * @package    Procvic
 * @subpackage Presenters
 */
final class ExercisePresenter extends BasePresenter
{
    /**
     * @var \Procvic\Model\Facades\CategoryFacade
     * @inject
     */
    public $categoryFacade;

    /**
     * @var \Procvic\Model\Facades\ExerciseFacade
     * @inject
     */
    public $exerciseFacade;

    /**
     * @var \Procvic\Model\ExerciseModel
     * @inject
     */
    public $exerciseModel;


    /**
     * @param int $id
     * @SuppressWarnings(PHPMD)
     */
    public function renderShow($id)
    {
        $this->template->exercise = $this->exerciseFacade->getById($id);
    }


    /**
     *
     */
    public function createComponentAddExerciseForm()
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        $items = $this->categoryFacade->getCategoriesForSelect();
        $form->addSelect('category', null, $items)
            ->setRequired('frontend-exercise.add.form_please_enter_category');

        $form->addTextarea('text')
            ->setRequired('frontend-exercise.add.form_please_enter_text');

        $form->addText('answer_correct')
            ->setRequired('frontend-exercise.add.form_please_enter_answer_correct');

        $form->addText('answer_bad_1')
            ->setRequired('frontend-exercise.add.form_please_enter_answer_bad_first');

        $form->addText('answer_bad_2');

        $form->addText('answer_bad_3');

        $form->addTextarea('note');

        $form->addSubmit('send', 'frontend-exercise.add.form_action_button');

        $form->onSuccess[] = $this->addExerciseFormSucceeded;

        return $form;
    }


    /**
     * @param \Nette\Application\UI\Form $form
     * @SuppressWarnings(PHPMD)
     */
    public function addExerciseFormSucceeded($form, $values)
    {
        $exercise = [
            'question' => [
                'text' => $values->text,
                'category' => $values->category,
                'note' => $values->note,
                'user' => $this->user->getId(),
                'level' => 'low',
            ],
            'answers' => [
                [
                    'text' => $values->answer_correct,
                    'correct' => true,
                ],
                [
                    'text' => $values->answer_bad_1,
                    'correct' => false,
                ],
            ],
        ];

        if (!empty($values->answer_bad_2)) {
            $exercise['answers'][] = [
                'text' => $values->answer_bad_2,
                'correct' => false,
            ];
        }

        if (!empty($values->answer_bad_3)) {
            $exercise['answers'][] = [
                'text' => $values->answer_bad_3,
                'correct' => false,
            ];
        }

        $this->exerciseModel->save($exercise);

        $this->flashMessage($this->translator->translate('frontend-exercise.add.flash_message'), 'success');
        $this->redirect('Homepage:');
    }
}
