<?php

namespace Procvic\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Procvic\ApplicationModule\Traits\DI\InjectTranslatorTrait;
use Procvic\Presenters\BasePresenter;
use Procvic\Model\UserManager;
use Procvic\ApplicationModule\Forms\ForgottenPasswordForm;
use Procvic\ApplicationModule\Forms\ChangePasswordForm;

/**
 * Forgotten password presenter
 *
 * @package Procvic
 * @subpackage ApplicationModule\Presenters
 */
final class ForgottenPasswordPresenter extends BasePresenter
{
    use InjectTranslatorTrait;

    /**
     * @var \Procvic\ApplicationModule\Model\Repositories\UserRepository
     * @inject
     */
    public $userManager;

    /**
     * @var \Procvic\ApplicationModule\Forms\ForgottenPasswordForm
     * @inject
     */
    public $passwordForm;

    /**
     * @var \Procvic\ApplicationModule\Forms\ChangePasswordForm
     * @inject
     */
    public $changePasswordForm;


    /**
     * @param string $email
     * @param string $hash
     */
    public function actionChange($email, $hash)
    {
        if (!$this->userManager->isValidChangeForgottenPasswordRequest($email, $hash)) {
            $this->flashMessage(
                $this->translator->translate(
                    'frontend-forgottenPassword.change.flash_forgotten_password_request_not_valid'
                ),
                'alert'
            );

            $this->redirect('Sign:in');
        }
    }


    /**
     * @return \Nette\Application\UI\Form
     */
    public function createComponentForgottenPasswordForm()
    {
        $form = $this->passwordForm->create();
        $form->setTranslator($this->translator);
        $form->onSuccess[] = function () {
            $this->flashMessage(
                $this->translator->translate(
                    'frontend-forgottenPassword.default.flash_email_with_instructions_successfuly_sent'
                ),
                'success'
            );

            $this->redirect('Sign:in');
        };

        return $form;
    }


    /**
     * @return \Nette\Application\UI\Form
     */
    public function createComponentChangePasswordForm()
    {
        $changePasswordForm = $this->changePasswordForm->create($this->getParam('email'));
        $changePasswordForm->setTranslator($this->translator);
        $changePasswordForm->onSuccess[] = function () {
            $this->flashMessage('Vaše heslo bylo úspěšně změněno.', 'success');
            $this->redirect('Sign:in');
        };

        return $changePasswordForm;
    }
}
