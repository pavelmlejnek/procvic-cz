<?php


namespace Procvic\Presenters;

use Nette\Application\UI\Form;

/**
 * Class ProfilePresenter
 *
 * @package Procvic\Presenters
 */
class ProfilePresenter extends BasePresenter
{
    /**
     * @var \Procvic\Model\Facades\UserFacade
     * @inject
     */
    public $userFacade;


    /**
     * @param $userId
     * @throws \Nette\Application\BadRequestException
     */
    public function actionDefault($userId)
    {
        if (!$user = $this->userFacade->getById($userId)) {
            $this->error();
        }

        $this->template->profile = $user;
    }


    /**
     * @throws \Nette\Application\BadRequestException
     */
    public function actionEdit()
    {
        if (!$this->user->isLoggedIn()) {
            $this->error();
        }
    }


    /**
     * @return \Nette\Application\UI\Form
     */
    public function createComponentEditProfile()
    {
        $user = $this->user->getIdentity();

        $form = new Form();
        $form->setTranslator($this->translator->domain('strings'));

        $form->addText('name', 'profileForm.name.label')
            ->setDefaultValue($user->name)
            ->addCondition(Form::FILLED)
            ->addRule(Form::MIN_LENGTH, 'profileForm.name.minLength', 2);

        $form->addText('surname', 'profileForm.surname.label')
            ->setDefaultValue($user->surname)
            ->addCondition(Form::FILLED)
            ->addRule(Form::MIN_LENGTH, 'profileForm.surname.minLength', 2);

        $form->addTextArea('about', 'profileForm.about.label')
            ->setDefaultValue($user->about)
            ->addCondition(Form::FILLED)
            ->addRule(Form::MIN_LENGTH, 'profileForm.about.minLength', 2);

        $form->addSubmit('send', 'profileForm.send.label');

        $form->onSuccess[] = function (Form $form) use ($user) {
            $this->userFacade->updateName($user->getId(), $form->values->name);
            $this->userFacade->updateSurname($user->getId(), $form->values->surname);
            $this->userFacade->updateAbout($user->getId(), $form->values->about);

            $this->logger->log(sprintf('User "%s" changed his profile.', $user->email), \Monolog\Logger::INFO);
            $this->flashMessage($this->translator->translate('strings.profile.changeProfile.success'), 'success');
            $this->redirect('this');
        };


        return $form;
    }


    /**
     * @return \Nette\Application\UI\Form
     */
    public function createComponentChangePassword()
    {
        $form = new Form();
        $form->setTranslator($this->translator->domain('strings'));
        $form->addPassword('old_password', 'profile.edit.changePassword.oldPassword.label')
            ->setRequired();

        $form->addPassword('new_password', 'profile.edit.changePassword.newPassword.label')
            ->addRule(Form::MIN_LENGTH, 'Min length', 4)
            ->setRequired();

        $form->addPassword('new_password_repeat', 'profile.edit.changePassword.newPasswordRepeat.label')
            ->addRule(Form::EQUAL, 'Equal', $form['new_password'])
            ->setRequired();

        $form->addSubmit('save', 'profile.edit.changePassword.save.value');

        $form->onSuccess[] = function (Form $form) {
            $email = $this->user->getIdentity()->email;
            $oldPassword = $form->values->old_password;
            $newPassword = $form->values->new_password;

            if ($this->userFacade->isValidPassword($email, $oldPassword)) {
                $this->userFacade->changePassword($email, $newPassword);

                $this->logger->log(sprintf('User "%s" changed his password.', $email), \Monolog\Logger::INFO);
                $this->flashMessage(
                    $this->translator->translate('strings.profile.edit.changePassword.success'),
                    'success'
                );
                $this->redirect('this');
            }
        };

        return $form;
    }
}
