<?php

namespace Procvic\Presenters;

use Nette;
use Procvic\Traits\TInjectTranslator;

/**
 * Base presenter for all application presenters.
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    use TInjectTranslator;

    /**
     * @var \Procvic\Components\SignControl\ISignControlFactory
     * @inject
     */
    public $signControlFactory;


    /**
     * @var \Tracy\ILogger
     * @inject
     */
    public $logger;


    /**
     * @todo Method getContext() is deprecated, we should rewrite this using another way of getting parameters.
     */
    protected function beforeRender()
    {
        parent::beforeRender();

        $hash = $this->getContext()->parameters['revision']['hash'];
        $localChanges = (bool) $this->getContext()->parameters['revision']['localChanges'];
        $this->template->gitRevision = $hash;
        $this->template->gitLocalChanges = $localChanges;
    }


    /**
     * @return \Nette\Application\UI\Form
     */
    public function createComponentSignControl()
    {
        $control = $this->signControlFactory->create();
        $control->onSignIn[] = function () {
            $this->redirect('this');
        };
        $control->onSignInError[] = function () {
            $this->flashMessage($this->translator->translate('strings.signInForm.validation.failed'), 'danger');
            $this->redirect('this');
        };
        $control->onSignUp[] = function () {
            $this->redirect(':Homepage:');
        };
        $control->onFacebookMissingEmailPermission = function () {
            $this->flashMessage($this->translator->translate('login.facebook.permission.missingEmail'), 'danger');
        };
        $control->onPasswordRecovery[] = function () {
            $this->flashMessage($this->translator->translate('strings.sign.recovery.success'), 'success');
            $this->redirect(':Homepage:');
        };
        $control->onPasswordRecoveryFailed[] = function () {
            $this->flashMessage($this->translator->translate('strings.sign.recovery.failed'), 'danger');
        };

        return $control;
    }


    public function actionSignOut()
    {
        $this->user->logout();

        $this->flashMessage(
            $this->translator->translate('strings.logout.success'),
            'success'
        );

        $this->redirect(':Homepage:default');
    }
}
