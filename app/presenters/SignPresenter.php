<?php

namespace Procvic\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Procvic\Traits\TRedirectSignedUser;

/**
 * Sign presenter
 *
 * @package    Procvic
 * @subpackage Presenters
 */
final class SignPresenter extends BasePresenter
{
    use TRedirectSignedUser;

    /**
     * @var \Procvic\Model\Facades\UserFacade
     * @inject
     */
    public $userFacade;


    /**
     * @param string $email
     * @param string $hash
     */
    public function actionNewPassword($email, $hash)
    {
        if (!$this->userFacade->isValidNewPasswordRequest($email, $hash)) {
            $this->flashMessage($this->translator->translate('strings.sign.recovery.newPassword.invalid'), 'alert');
            $this->redirect(':Homepage:');
        }
    }


    /**
     * @return \Nette\Application\UI\Form
     */
    public function createComponentNewPasswordForm()
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        $form->addHidden('email', $this->getParameter('email')); // @todo possible security problem?
        $form->addPassword('password', 'strings.sign.recovery.newPassword.password.label')
            ->addRule(Form::MIN_LENGTH, 'strings.sign.recovery.newPassword.password.minLength', 4)
            ->setRequired('strings.sign.recovery.newPassword.password.required');
        $form->addPassword('password_repeat', 'strings.sign.recovery.newPassword.passwordRepeat.label')
            ->addRule(Form::EQUAL, 'strings.sign.recovery.newPassword.passwordRepeat.equal', $form['password'])
            ->setRequired('strings.sign.recovery.newPassword.passwordRepeat.required');
        $form->addSubmit('send', 'strings.sign.recovery.newPassword.send.label');

        $form->onSuccess[] = function (Form $form) {
            $this->userFacade->changeForgottenPassword($form->values->email, $form->values->password);

            $this->logger->log(
                sprintf('User "%s" changed forgotten password.', $form->values->email),
                \Monolog\Logger::INFO
            );
            $this->presenter->flashMessage(
                $this->translator->translate('strings.sign.recovery.newPassword.success'),
                'success'
            );
            $this->presenter->redirect(':Homepage:');
        };

        return $form;
    }
}
