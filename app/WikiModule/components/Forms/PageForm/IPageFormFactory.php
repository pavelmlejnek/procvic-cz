<?php

namespace Procvic\WikiModule\Components\Forms\PageForm;

/**
* @package    Procvic
* @subpackage WikiModule\Components\Forms\PageForm
*/
interface IPageFormFactory
{
    /**
     * @return \Procvic\WikiModule\Components\Forms\PageForm\PageForm
     */
    public function create();
}
