<?php

namespace Procvic\WikiModule\Components\Forms\PageForm;

use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Procvic\WikiModule\Model\Facades\PageFacade;

/**
* @package    Procvic
* @subpackage WikiModule\Components\Forms\PageForm
*/
final class PageForm extends Control
{
    /**
     * @var \Procvic\WikiModule\Model\Facades\PageFacade
     */
    private $pageFacade;


    /**
     * @var array
     */
    private $defaults;


    /**
     * @param \Procvic\WikiModule\Model\Facades\PageFacade $pageFacade
     */
    public function __construct(PageFacade $pageFacade)
    {
        $this->pageFacade = $pageFacade;
    }


    /**
     * @param array $defaults
     */
    public function setDefaults(array $defaults)
    {
        $this->defaults = $defaults;
    }


    /**
     * @return \Nette\Application\UI\Form
     */
    public function createComponentForm()
    {
        $form = new Form();

        $form->addHidden('slug');

        $form->addText('title', 'Titulek')
            ->addRule(Form::FILLED, 'Musíte vyplnit titulek stránky.')
            ->addRule(FORM::MIN_LENGTH, 'Titulek stránky je příliš krátký.', 2)
            ->setRequired();

        $form->addTextarea('content', 'Obsah')
            ->addRule(Form::FILLED, 'Musíte vyplnit obsah stránky.')
            ->setRequired();

        $form->addText('edit_summary', 'Shrnutí editace');

        if (!$this->defaults) {
            $form['edit_summary']->setDefaultValue('vytvořeno');
        } else {
            $form->setDefaults($this->defaults);
        }

        $form->addSubmit('save', 'Uložit');

        $form->onSuccess[] = $this->formSubmitted;

        return $form;
    }


    /**
     * @param \Nette\Application\UI\Form $form
     */
    public function formSubmitted(Form $form)
    {
        $values = $form->getValues();
        $userId = $this->presenter->user->identity->getId();
        $slug = \Nette\Utils\Strings::webalize($values->title);

        if ($values->slug) {
            $this->pageFacade->update(
                $values->title,
                $values->content,
                $values->edit_summary,
                $values->slug,
                $userId
            );

            $slug = $values->slug;

            $this->presenter->flashMessage('Úspěšně jste upravil(a) stránku', 'success');
        } else {
            $this->pageFacade->insert($values->title, $slug, $values->content, $values->edit_summary, $userId);
            $this->presenter->flashMessage('Úspěšně jste vytvořil(a) stránku', 'success');
        }

        $this->presenter->redirect(':Wiki:Page:show', [
            'slug' => $slug
        ]);
    }


    public function render()
    {
        $this->template->setFile(__DIR__ . '/pageForm.latte');
        $this->template->render();
    }
}
