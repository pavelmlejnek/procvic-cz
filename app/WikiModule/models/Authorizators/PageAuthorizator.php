<?php

namespace Procvic\WikiModule\Model\Authorizators;

use Nette\Object;
use Nette\Security\User;
use Procvic\WikiModule\Model\Entities\Page;

/**
 * Class WikiAuthorization
 *
 * @package Procvic\WikiModule\Model\Authorizators
 * @SuppressWarnings(PHPMD)
 */
class PageAuthorizator extends Object
{
    const PAGE_UNPROTECTED = 1;
    const PAGE_PROTECTED = 2;

    /**
     * @var \Nette\Security\User
     */
    private $user;


    /**
     * @param \Nette\Security\User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }


    /**
     * @param  \Procvic\WikiModule\Model\Entities\Page $page
     * @param  string                                  $action
     * @return boolean
     */
    public function isAllowed(Page $page, $action)
    {
        if ($page->protection == self::PAGE_PROTECTED) {
            return $this->user->isInRole('admin');
        } elseif ($page->protection == self::PAGE_UNPROTECTED) {
            return $this->user->isLoggedIn();
        }

        return false;
    }
}
