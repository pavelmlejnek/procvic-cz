<?php

namespace Procvic\WikiModule\Model\Entities;

use Nette\Database\Table\ActiveRow;
use Nette\Object;

/**
 * Class Revision
 *
 * @package Procvic\WikiModule\Model\Entities
 * @SuppressWarnings(PHPMD)
 */
class Revision extends Object
{
    /**
     * @var integer
     */
    public $id;


    /**
     * @var integer
     */
    public $userId;


    /**
     * @var string
     */
    public $title;


    /**
     * @var string
     */
    public $content;


    /**
     * @var string
     */
    public $editSummary;


    /**
     * @var \Nette\Utils\DateTime
     */
    public $createdAt;


    /**
     * @param \Nette\Database\Table\ActiveRow $row
     * @return static
     */
    public static function fromActiveRow(ActiveRow $row)
    {
        $page = new static;

        $page->id = $row->id;
        $page->userId = $row->user_id;
        $page->title = $row->title;
        $page->content = $row->content;
        $page->editSummary = $row->edit_summary;
        $page->createdAt = $row->created_at;

        return $page;
    }
}
