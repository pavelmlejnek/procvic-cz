<?php

namespace Procvic\WikiModule\Model\Entities;

use Nette\Database\Table\ActiveRow;
use Nette\Object;
use Procvic\WikiModule\Model\Repositories\PagesRevisionsRepository;
use Procvic\WikiModule\Model\Repositories\RevisionRepository;

/**
 * Class Page
 *
 * @package Procvic\WikiModule\Model\Entities
 * @SuppressWarnings(PHPMD)
 */
class Page extends Object
{
    /**
     * @var integer
     */
    public $id;


    /**
     * @var string
     */
    public $slug;


    /**
     * @var \Nette\Utils\DateTime
     */
    public $createdAt;


    /**
     * @var string
     */
    public $title;


    /**
     * @var Revision
     */
    public $latestRevision;


    /**
     * @var array
     */
    public $revisions = [];


    /**
     * @var integer
     */
    public $protection;


    /**
     * @param \Nette\Database\Table\ActiveRow $row
     * @return static
     */
    public static function fromActiveRow(ActiveRow $row)
    {
        $page = new static;
        $page->id = $row->id;
        $page->slug = $row->slug;
        $page->createdAt = $row->created_at;
        $page->protection = $row->protection;

        foreach ($row->related(RevisionRepository::$table, 'page_id') as $revisionRow) {
            $page->revisions[$revisionRow->id] = Revision::fromActiveRow($revisionRow);
        }

        $page->latestRevision = $page->revisions[$row->latest_revision_id];

        return $page;
    }
}
