<?php

namespace Procvic\WikiModule\Model\Repositories;

use Procvic\Model\Repositories\BaseRepository;

/**
* @package    Procvic
* @subpackage WikiModule\Model\Repositories
*/
class PageRepository extends BaseRepository
{
    /**
     * @var string
     */
    public static $table = 'pages';


    /**
     * @param  string $slug
     * @return mixed
     */
    public function getBySlug($slug)
    {
        return $this->findAll()->where('slug', $slug)->fetch();
    }


    /**
     * @param string  $slug
     * @param integer $revisionId
     */
    public function getByRevision($slug, $revisionId)
    {
        $query = 'SELECT p.*, r.id AS shown_revision_id, r.title, r.content,
                r.edit_summary, r.created_at AS last_revision_created_at
            FROM pages p
                LEFT JOIN revisions r ON r.id = ?
            WHERE p.slug = ?';

        return $this->database->query($query, $revisionId, $slug)->fetch();
    }


    /**
     * @param array  $data
     * @param string $slug
     */
    public function updateBySlug($data, $slug)
    {
        $this->findAll()->where('slug', $slug)->update($data);
    }
}
