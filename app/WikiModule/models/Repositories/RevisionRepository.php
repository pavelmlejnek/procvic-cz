<?php

namespace Procvic\WikiModule\Model\Repositories;

use Procvic\Model\Repositories\BaseRepository;

/**
* @package    Procvic
* @subpackage WikiModule\Model\Repositories
*/
class RevisionRepository extends BaseRepository
{
    /**
     * @var string
     */
    public static $table = 'revisions';


    /**
     * @param  string $slug
     * @return array
     */
    public function findBySlug($slug)
    {
        $query = 'SELECT c.page_id, r.*, u.email, u.name, u.surname
            FROM pages_revisions c
                LEFT JOIN revisions r ON c.revision_id = r.id
                LEFT JOIN users u ON r.user_id = u.id
            WHERE c.page_id = (SELECT p.id FROM pages p WHERE p.slug = ?)
            ORDER BY r.created_at DESC';

        return $this->database->query($query, $slug)->fetchAll();
    }


    /**
     * @param string  $slug
     * @param integer $revision
     */
    public function isValid($slug, $revision)
    {
        $query = 'SELECT * FROM pages_revisions c
            WHERE c.page_id = (SELECT p.id FROM pages p WHERE p.slug = ?) AND
                c.revision_id = (SELECT r.id FROM revisions r WHERE r.id = ?)';

        return $this->database->query($query, $slug, $revision)->fetch();
    }
}
