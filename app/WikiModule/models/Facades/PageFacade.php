<?php

namespace Procvic\WikiModule\Model\Facades;

use Nette\Object;
use Nette\Utils\DateTime;
use Procvic\WikiModule\Model\Repositories\PageRepository;
use Procvic\WikiModule\Model\Repositories\RevisionRepository;

/**
 * @package    Procvic
 * @subpackage WikiModule\Model\Managers
 * @SuppressWarnings(PHPMD)
 */
class PageFacade extends Object
{
    /**
     * @var \Procvic\WikiModule\Model\Repositories\PageRepository
     */
    private $pageRepository;


    /**
     * @var \Procvic\WikiModule\Model\Repositories\RevisionRepository
     */
    private $revisionRepository;


    /**
     * @param \Procvic\WikiModule\Model\Repositories\PageRepository     $pageRepository
     * @param \Procvic\WikiModule\Model\Repositories\RevisionRepository $revisionRepository
     */
    public function __construct(PageRepository $pageRepository, RevisionRepository $revisionRepository)
    {
        $this->pageRepository = $pageRepository;
        $this->revisionRepository = $revisionRepository;
    }


    /**
     * @param  string $slug
     * @return \Procvic\WikiModule\Model\Entities\Page
     */
    public function getPageBySlug($slug)
    {
        return $this->pageRepository->getBySlug($slug);
    }


    /**
     * @param $slug
     * @return array
     */
    public function getRevisions($slug)
    {
        return $this->revisionRepository->findBySlug($slug);
    }


    /**
     * @param string  $slug
     * @param integer $revisionId
     */
    public function getPageRevision($slug, $revision)
    {
        return $this->pageRepository->getByRevision($slug, $revision);
    }


    /**
     * @param  string  $slug
     * @param  integer $revision
     * @return boolean
     */
    public function isValidPageRevision($slug, $revision)
    {
        return $this->revisionRepository->isValid($slug, $revision);
    }


    /**
     * @todo Add transaction for search latest_revision_id, for example:
     *       TRANSANCTION; INSERT id, ..., MAX(id)+1 as latest_revision_id ....; COMMIT;
     * @todo In database schema set latest_revision_id as NOT NULL
     * @param string $title
     * @param string $content
     * @param string $editSummary
     */
    public function insert($title, $slug, $content, $editSummary, $userId)
    {
        $page = $this->pageRepository->insert([
            'slug' => $slug,
            'created_at' => new DateTime(),
        ]);

        $revision = $this->createPageRevision($title, $content, $page->id, $userId, $editSummary);

        $this->pageRepository->updateBySlug(['latest_revision_id' => $revision->id,], $slug);
    }


    /**
     * @param string $title
     * @param string $content
     * @param string $editSummary
     * @param string $slug
     */
    public function update($title, $content, $editSummary, $slug, $userId)
    {
        $page = $this->pageRepository->getBy(['slug' => $slug]);
        $revision = $this->createPageRevision($title, $content, $page->id, $userId, $editSummary);

        $this->pageRepository->updateBySlug(['latest_revision_id' => $revision->id,], $slug);
    }


    /**
     * @param  string $title
     * @param  string $content
     * @param  string $editSummary
     * @return \Nette\Database\Row
     */
    private function createPageRevision($title, $content, $pageId, $userId, $editSummary = null)
    {
        return $this->revisionRepository->insert([
            'user_id' => $userId,
            'page_id' => $pageId,
            'title' => $title,
            'content' => $content,
            'edit_summary' => $editSummary,
            'created_at' => new DateTime(),
        ]);
    }
}
