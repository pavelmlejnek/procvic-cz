<?php

namespace Procvic\WikiModule\Parser;

/**
 * Interface IParser
 *
 * @package   Procvic
 * @subpackage WikiModule\Parser
 */
interface IParser
{
    /**
     * @param  string $string
     * @return string
     */
    public function parse($string);
}
