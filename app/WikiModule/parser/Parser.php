<?php

namespace Procvic\WikiModule\Parser;

use Nette;
use Nette\Object;

/**
 * Class Parser
 *
 * @package    Procvic
 * @subpackage WikiModule\Parser
 */
class Parser extends Object
{

    /**
     * @var array
     */
    private $parsers = [];

    /**
     * @param \Procvic\WikiModule\Parser\IParser $parser
     */
    public function register(IParser $parser)
    {
        $this->parsers[] = $parser;
    }


    /**
     * @param  string $string
     * @return string
     */
    public function parse($string)
    {
        foreach ($this->parsers as $parser) {
            $string = $parser->parse($string);
        }

        return $string;
    }
}
