<?php

namespace Procvic\WikiModule\Parser\Parsers;

use Nette;
use Nette\Object;
use Michelf\MarkdownExtra;
use Procvic\WikiModule\Parser\IParser;
use HTMLPurifier_Config;
use HTMLPurifier;

/**
 * Class MarkdownParser
 *
 * @package    Procvic
 * @subpackage WikiModule\Parser\Parsers
 */
class MarkdownParser extends Object implements IParser
{

    /**
     * @var \Michelf\MarkdownExtra
     */
    private $markdown;

    /**
     * @var \HTMLPurifier
     */
    private $htmlPurifier;


    /**
     * @param string $cachePath
     */
    public function __construct($cachePath)
    {
        $htmlPurifierConfig = HTMLPurifier_Config::createDefault();
        $htmlPurifierConfig->set('Cache.SerializerPath', $cachePath);

        $this->htmlPurifier = new HTMLPurifier($htmlPurifierConfig);
        $this->markdown = new MarkdownExtra();
    }


    /**
     * @param  string $string
     * @return string
     */
    public function parse($string)
    {
        return $this->htmlPurifier->purify($this->markdown->transform($string));
    }
}
