<?php

namespace Procvic\WikiModule\Parser\Parsers;

use Nette;
use Nette\Object;
use Nette\Utils\Strings;
use Nette\Application\LinkGenerator;
use Procvic\WikiModule\Parser\IParser;

/**
 * Class IntertextualityParser
 *
 * @package    Procvic
 * @subpackage WikiModule\Parser\Parsers
 * @todo       Check if page exists and add class depending on it
 */
class IntertextualityParser extends Object implements IParser
{

    /**
     * @var \Nette\Application\LinkGenerator
     */
    private $linkGenerator;

    /**
     * @param \Nette\Application\LinkGenerator $linkGenerator
     */
    public function __construct(LinkGenerator $linkGenerator)
    {
        $this->linkGenerator = $linkGenerator;
    }


    /**
     * @param  string $string
     * @return string
     */
    public function parse($string)
    {
        return Strings::replace($string, '{^[ ]{0,3}\[(.+)\][ ]?["{](.*?)["}]{1}}xm', function($l) {
            return '[' . $l[2] . '](' . $this->linkGenerator->link('Wiki:Page:show', ['slug' => $l[1]]) . ')';
        });
    }
}
