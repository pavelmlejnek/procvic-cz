<?php

namespace Procvic\WikiModule\Presenters;

use Procvic\Presenters\BasePresenter;
use Procvic\WikiModule\Model\Entities\Page;

/**
 * @package    Procvic
 * @subpackage WikiModule\Presenters
 * @SuppressWarnings(PHPMD)
 */
class PagePresenter extends BasePresenter
{
    /**
     * @var string
     * @persistent
     */
    public $slug;


    /**
     * @var \Procvic\WikiModule\Model\Facades\PageFacade
     * @inject
     */
    public $pageFacade;


    /**
     * @var \Procvic\WikiModule\Components\Forms\PageForm\IPageFormFactory
     * @inject
     */
    public $pageFormFactory;


    /**
     * @var \Procvic\WikiModule\Parser\Parser
     * @inject
     */
    public $parser;


    /**
     * @var \Procvic\WikiModule\Model\Authorizators\PageAuthorizator
     * @inject
     */
    public $pageAuthorizator;


    /**
     * @var \Procvic\WikiModule\Model\Entities\Page
     */
    public $page;



    public function startup()
    {
        parent::startup();

        if (!$page = $this->pageFacade->getPageBySlug($this->slug)) {
            $this->flashMessage($this->translator->translate('strings.wiki.page.doesntExists'), 'info');
            $this->redirect(':Wiki:CreatePage:');
        }

        $this->page = Page::fromActiveRow($page);
    }


    /**
     * @return \Nette\Application\UI\ITemplate
     */
    protected function createTemplate()
    {
        $template = parent::createTemplate();

        $template->addFilter('parse', function ($string) {
            return $this->parser->parse($string);
        });

        return $template;
    }


    /**
     * @param string $slug
     */
    public function renderShow($slug)
    {
        $this->template->page = $this->page;
    }


    /**
     * @param string $slug
     */
    public function actionEdit($slug)
    {
        if (!$this->pageAuthorizator->isAllowed($this->page, 'edit')) {
            $this->flashMessage($this->translator->translate('strings.wiki.page.edit.missingPermission'), 'danger');
            $this->redirect('show', ['slug' => $slug]);
        }

        $this->template->page = $this->page;

        $this['pageForm']->setDefaults([
            'slug' => $slug,
            'title' => $this->page->latestRevision->title,
            'content' => $this->page->latestRevision->content
        ]);
    }


    /**
     * @param string $slug
     */
    public function actionRevisions($slug)
    {
        $this->template->page = $this->page;
    }


    /**
     * @return \Procvic\WikiModule\Components\Forms\PageForm\PageForm
     */
    public function createComponentPageForm()
    {
        return $this->pageFormFactory->create();
    }
}
