<?php

namespace Procvic\WikiModule\Presenters;

use Procvic\Presenters\BasePresenter;

/**
 * @package    Procvic
 * @subpackage WikiModule\Presenters
 */
class CreatePagePresenter extends BasePresenter
{
    /**
     * @var \Procvic\WikiModule\Components\Forms\PageForm\IPageFormFactory
     * @inject
     */
    public $pageFormFactory;


    /**
     * @throws \Nette\Application\BadRequestException
     */
    public function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            $this->error('This page does not exists');
        }
    }


    /**
     * @return \Procvic\WikiModule\Components\Forms\PageForm\PageForm
     */
    public function createComponentPageForm()
    {
        return $this->pageFormFactory->create();
    }
}
